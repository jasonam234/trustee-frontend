import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import { Col, Row } from "react-bootstrap";

const AsuransiSectionSkeleton = () => {
  return (
    <div>
      <Row className="rowCenter">
        <h4>
          <Skeleton />
        </h4>
        <hr className="garisPembatas" />
        <Col>
          <Skeleton
            className="imagePremi mx-auto d-block"
            width="286px"
            height="429px"
          />
        </Col>
        <Col>
          <p>
            <Skeleton />
          </p>
        </Col>
        <br />
      </Row>
    </div>
  );
};

export default AsuransiSectionSkeleton;
