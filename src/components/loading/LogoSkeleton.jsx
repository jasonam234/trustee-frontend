import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import logo from "../../logo.png";

const LogoSkeleton = () => {
  return (
    <div className="text-center">
         <Skeleton />
       <img className="w-100% m-auto"  src={logo} alt="logo" />
       <Skeleton />
    </div>
  );
};

export default LogoSkeleton;
