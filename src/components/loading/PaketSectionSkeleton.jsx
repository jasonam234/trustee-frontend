import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";
import { CardBody, CardFooter, CardHeader, CardText } from "reactstrap";
import { Container, Row, Col, Card, Button } from "react-bootstrap";

const PaketSectionSkeleton = () => {
  return (
    <div>
      <Row className="rowCenter">
        <h4><Skeleton/> </h4>
        <hr className="garisPembatas" />
      </Row>
      <Row className="rowCenter">
        <Col>
          <Card>
            <CardBody>
              <CardHeader>
                <Skeleton />
              </CardHeader>
              <CardText>
                <Skeleton />
              </CardText>
              <CardFooter>
                {/* <Button> */}
                  <Skeleton />
                {/* </Button> */}
              </CardFooter>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default PaketSectionSkeleton;
