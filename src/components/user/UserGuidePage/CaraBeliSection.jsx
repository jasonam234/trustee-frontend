import { Card, Container, Row } from "react-bootstrap";
import "../../../assets/user/UserGuide.css";
import searchPic from "../../../assets/img/search.png";
import formPic from "../../../assets/img/contact-form.png";
import profilPic from "../../../assets/img/profile.png";
import checkPic from "../../../assets/img/checked.png";

const CaraBeliSection = () => {
  return (
    <div>
      <Container fluid>
        <Row className="rowCenter">
          <h4>Cara Beli</h4>
          <hr className="garisPembatas" />
          <br />
        </Row>
        <Row style={{ justifyContent: "center" }}>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={searchPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Cek Premi</Card.Title>
              <Card.Text className="cardText">
                Cek Premi yang ada pada website kami, kemudian tentukan paket
                yang telah kami sediakan, lalu pilih jangka waktu periode ingin
                berlangganan
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={formPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Isi Form</Card.Title>
              <Card.Text className="cardText">
                Setelah memilih paket dan jenis premi, anda bisa langsung
                mengisi form yang telah kami sediakan, lalu pilih ajukan
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={profilPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Ke Halaman Profil</Card.Title>
              <Card.Text className="cardText">
                Pergi ke halaman profil, jika status yang ada pada premi adalah
                pending, maka user harus menunggu konfirmasi dari kami, jika
                sudah menjadi Menunggu Pembayaran, user dapat langsung
                membayarkan premi nya agar premi tersebut menjadi aktif
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={checkPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Pembelian Berhasil</Card.Title>
              <Card.Text className="cardText">
                Setelah berhasil melakukan pembayaran, maka status anda akan
                menjadi aktif, dan asuransi kami siap untuk melindungi anda
              </Card.Text>
            </Card.Body>
          </Card>
        </Row>
      </Container>
    </div>
  );
};

export default CaraBeliSection;
