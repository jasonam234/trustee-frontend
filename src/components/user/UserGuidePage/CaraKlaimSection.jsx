import { Container, Row, Card } from "react-bootstrap";
import "../../../assets/user/UserGuide.css";
import checkPic from "../../../assets/img/checked.png";
import searchPic from "../../../assets/img/search.png";
import formPic from "../../../assets/img/contact-form.png";
import waitPic from "../../../assets/img/chronometer.png";

const CaraKlaimSection = () => {
  return (
    <div>
      <Container fluid>
        <Row className="rowCenter">
          <h4>Cara Klaim</h4>
          <hr className="garisPembatas" />
        </Row>
        <Row style={{ justifyContent: "center" }}>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={searchPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Cek Premi</Card.Title>
              <Card.Text className="cardText">
                Cek premi pada halaman profil dengan statusnya adalah
                <b> Aktif</b>
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={formPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Isi Form</Card.Title>
              <Card.Text className="cardText">
                Setelah memilih premi, maka anda akan diminta untuk memasukan
                bukti foto seperti pembayaran, sertifikat kematian atau bukti
                bukti lainnya saat mengajukan klaim premi
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={waitPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Konfirmasi Admin</Card.Title>
              <Card.Text className="cardText">
                Setelah anda mengajukan klaim, maka anda harus menunggu
                konfirmasi dari kami mengenai penerimaan dan penolakan dari
                pengajuan klaim anda, yang akan kami kirim kan melalui email
              </Card.Text>
            </Card.Body>
          </Card>
          <Card className="cardBeli">
            <div className="imgCenter">
              <Card.Img variant="top" src={checkPic} className="imgBeli" />
            </div>
            <Card.Body>
              <Card.Title>Klaim Berhasil</Card.Title>
              <Card.Text className="cardText">
                Setelah berhasil melakukan pembayaran, maka status anda akan
                menjadi aktif, dan asuransi kami siap untuk melindungi anda
              </Card.Text>
            </Card.Body>
          </Card>
        </Row>
      </Container>
    </div>
  );
};

export default CaraKlaimSection;
