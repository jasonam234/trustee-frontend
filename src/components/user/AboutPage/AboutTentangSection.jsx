import { Col, Row } from "reactstrap";
import LogoSkeleton from "../../loading/LogoSkeleton";
import { Card } from "react-bootstrap";
import cheapPic from "../../../assets/img/loss.png";
import choicePic from "../../../assets/img/multiple.png";
import payPic from "../../../assets/img/debit-card.png";

const AboutTentangSection = () => {
  return (
    <div>
      <Row className="rowCenter">
        <h4 className="text-secondary textAboutHead">Tentang Kami</h4>
      </Row>
      <br />
      <Row>
        <Col>
          <img
            src="https://images.unsplash.com/photo-1600880292089-90a7e086ee0c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80"
            alt=""
            width="50%"
            className="imgAbout"
          />
        </Col>
        <Col style={{marginRight:"10%"}}>
          <h5 className="text-identity fs-1 fw-bold">Trustee</h5>
          <span className="contentCenter">
            Trustee merupakan perusahaan asuransi yang baru saja dibentuk
            sekitar akhir tahun 2021. Trustee hadir dengan tujuan untuk
            membangun minat para kaum muda untuk melindungi dirinya dengan
            asuransi.
          </span>
          <Row>
            <h4 className="text-secondary textAboutHead" >Layanan Inti Kami</h4>
            <Col lg={4}>
              <Card className="cardBeli">
                <div className="imgCenter">
                  <Card.Img variant="top" src={choicePic} className="imgBeli" />
                </div>
                <Card.Body>
                  <Card.Title className="text-secondary">Tersedia Banyak Premi</Card.Title>
                  <Card.Text className="cardText"></Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={4}>
              <Card className="cardBeli">
                <div className="imgCenter">
                  <Card.Img variant="top" src={payPic} className="imgBeli" />
                </div>
                <Card.Body>
                  <Card.Title className="text-secondary">Metode Pembayaran <br /> Sekali</Card.Title>
                  <Card.Text className="cardText"></Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={4}>
              <Card className="cardBeli">
                <div className="imgCenter">
                  <Card.Img variant="top" src={cheapPic} className="imgBeli" />
                </div>
                <Card.Body>
                  <Card.Title className="text-secondary">Harga Terjangkau</Card.Title>
                  <Card.Text className="cardText"></Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
      <br />
    </div>
  );
};

export default AboutTentangSection;
