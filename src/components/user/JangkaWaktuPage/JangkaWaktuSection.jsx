import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import "../../../assets/user/JangkaWaktuSection.css";
import { Button, Card, Row, Col, Container } from "react-bootstrap";
import { getData, getDataNoToken } from "../../../api/EndPointAuth";
import swal from "sweetalert";
import LogoSkeleton from "../../loading/LogoSkeleton";
import { useDispatch } from "react-redux";
import calenderPic from "../../../assets/img/timetable.png";
import LoadingSpin from "../../loader/LoadingSpin";

const JangkaWaktuSection = () => {
  let param = useParams();
  let navigate = useNavigate();
  let dispatch = useDispatch();
  let jenis_id = param.premi_id;
  let paket_name = param.paket_name;
  const [paket, setPaket] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(async () => {
    await getDataNoToken(`/premi/get/paket/${jenis_id}/${paket_name}`)
      .then((res) => {
        let data = res.data.data;
        setPaket(data);
      })
      .catch((err) => {
        swal("Gagal!", "Tidak bisa memilih sebelum login", "error");
        navigate("/");
      });
    setIsLoading(false);
  }, [setPaket]);
  const jangkaWaktu = (id) => {
    const token = localStorage.getItem("token");
    if (token === null) {
      swal("Gagal!", "Silakan login terlebih dahulu!", "error");
    } else {
      navigate(
        { pathname: `/asuransi/form/${jenis_id}` },
        {
          state: {
            paket_id: id,
          },
        }
      );
      window.location.reload();
    }
  };
  console.log(paket);
  if (isLoading === true) {
    return <LoadingSpin />;
  }
  return (
    <Container fluid>
      <div className="rowCenter">
        <h4>Pilih jangka waktu : </h4>
        <Row className="rowCenterJWS">
          {paket.map((value, index) => {
            return (
              <Col lg={4}>
                <Card className="cardJWS">
                  <div className="imgCenter">
                    <Card.Img src={calenderPic} className="imgBeli" />
                  </div>
                  <Card.Body> Lama Periode : {value.period} Bulan </Card.Body>
                  <Card.Footer>
                    <Button
                      className="btnJngka"
                      onClick={() => jangkaWaktu(value.paket_id)}
                    >
                      Pilih
                    </Button>
                  </Card.Footer>
                </Card>
              </Col>
            );
          })}
        </Row>
      </div>
    </Container>
  );
};

export default JangkaWaktuSection;
