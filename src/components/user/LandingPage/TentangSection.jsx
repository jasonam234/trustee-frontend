import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Col, Container, Row } from "reactstrap";
import { getDataNoToken } from "../../../api/EndPointAuth";
import gambarLanding from "../../../assets/img/12.jpg"

const TentangSection = () => {
  const [asuransi, setAsuransi] = useState([]);
  useEffect(() => {
    getDataNoToken(`/premi/get/jenis`).then((res) => {
      console.log(res.data.data);
      let data = res.data.data;
      setAsuransi(data);
    });
  },[setAsuransi]);

  return (
    <div>
      <Container fluid>
      <h5 className="mt-5 text-center text-secondary">Pilih Asuransi sesuai dengan kebutuhan anda</h5>
      <h3 className="mt-5 text-center text-secondary p-5"><span className="text-identity fs-1 fw-bold">Trustee</span> hadir untuk memberikan proteksi dalam kehidupan kaum muda dan keluarga anda</h3>
        <Row className="mt-5  bg-identity">
         
          <Col lg={6}>
            <div className="img-rounded ">
            <img
              className="w-100 p-5"
              style={{ marginLeft: "20px" }}
              src={gambarLanding}
              alt=""
            />
            </div>
          
          </Col>
          <Col lg={6} className="p-5 text-white bg-identity">
            <h1 className="fw-bold">Mengapa Asuransi Penting ?</h1>
            <p className="fs-4">
              Asuransi sangat penting untuk melindungi anda dari berbagai macam
              hal yang tidak di duga, dengan Asuransi dari kami, kami dapat
              membantu anda dalam mengatasi hal - hal terduga dengan biaya yang
              sangat terjangkau
            </p>
            
          </Col>
        </Row>
        <Row>
          <Col lg={6} className="p-5 ">
          <h1 className="fw-bold text-secondary">Berikan Proteksi diri anda dan keluarga</h1>
            <ul className="fs-4 text-secondary">
              {asuransi.map((value) => {
                return <li> 
                <Link to={"asuransi/"+value.jenis_id} className="nav-link text-secondary">
                  {value.jenis_name}</Link></li>;
              })}
            </ul>
          </Col>
          <Col lg={6}>
          <div className="img-rounded">
            <img
              className="w-100 p-5 "
              src="https://img.okezone.com/content/2014/09/18/196/1040811/cUrTquhYgY.jpg"
              alt=""
            />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
export default TentangSection;
