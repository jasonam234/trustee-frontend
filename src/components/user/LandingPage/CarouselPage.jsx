import { Carousel } from "react-bootstrap";
import "../../../assets/user/LandingPage.css"

const CarouselPage = () => {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="http://localhost:8081/api/admin/cms/image/carousel_one.jpg"
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="http://localhost:8081/api/admin/cms/image/carousel_two.jpg"
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="http://localhost:8081/api/admin/cms/image/carousel_three.jpg"
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
  );
};

export default CarouselPage;
