import { Button} from "react-bootstrap";
import { Col, Container, Row } from "reactstrap";
import { FiPhoneCall } from "react-icons/fi";
import { HiOutlineMail } from "react-icons/hi";
const CallToActionSection = () => {
  return (
    <div>
      <Container fluid>
        <div  className="text-center text-secondary ">
        <h5 className="mt-5 text-center text-secondary">Apakah anda menemui kesulitan atau butuh bantuan dari kami?</h5>
      <h3 className="mt-5 text-center text-secondary  p-5"><span className="text-identity fs-1 fw-bold">Trustee Care</span> akan siap membantu anda kapan pun anda membutuhkan kami</h3>
      <p className=" w-75 m-auto">Anda dapat menghubungi <span className="text-identity fw-bold">Trustee Care</span> untuk mengenal lebih dalam dengan produk kami atau berkonsultasi untuk memilih produk yang tepat sesuai dengan kebutuhan anda.</p>
        </div>
      <Row className="mt-5 m-auto">
         <Col className="text-center">
         <Button  className="bg-identity  shadow-lg p-3 mb-5 rounded"> <a href="tel:085779995638" className="text-white text-decoration-none"><FiPhoneCall size={23}></FiPhoneCall> Trustee Care</a></Button>
         </Col>
         <Col className="text-center ">
         <Button  className="bg-identity shadow-lg p-3 mb-5 rounded "> <a href="mailto:cs@trustee.com" className="text-white text-decoration-none"><HiOutlineMail size={25}></HiOutlineMail> Trustee Care</a></Button>
         </Col>
        </Row>
      </Container>
    </div>
  );
};
export default CallToActionSection;
