import "../../../assets/user/LandingPage.css";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { Col, Row, Container } from "reactstrap";
import { useEffect, useState } from "react";
import PaketSectionSkeleton from "../../loading/PaketSectionSkeleton";

const DiscountSection = () => {
  const [isLoading, setisLoading] = useState(false);
  useEffect(() => {
    setisLoading(true);
    setTimeout(() => {
      setisLoading(false);
    }, 2000);
  }, isLoading);

  if (isLoading === true) {
    return <PaketSectionSkeleton />;
  }
  return (
    <div>
      <Container fluid>
        <br />
        <Row className="rowCenter">
          <h3>Harga Diskon</h3>
          <hr className="garisPembatas" />
        </Row>

        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            "& > :not(style)": {
              m: 1,
              width: 1080,
              height: 100,
            },
            borderRadius: 1,
          }}
          style={{ justifyContent: "center" }}
        >
          <Paper elevation={4}>
            <div className="tinggiPaper">
              <div className="paperRight">
                <div className="paperLeft">
                  <div style={{ textAlign: "left", margin: "14px" }}>
                    <h3>Diskon 5%</h3>
                    <p>Berlaku untuk anda yang berumur dibawah 24 Tahun!</p>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
          <Paper elevation={4}>
            <div className="tinggiPaper">
              <div className="paperContent2">
                <div className="paperContent">
                  <div style={{ textAlign: "right", margin: "14px" }}>
                    <h3>Diskon 3%</h3>
                    <p>Berlaku untuk anda yang berumur antara 24 - 29 Tahun!</p>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
          <Paper elevation={4}>
            <div className="tinggiPaper">
              <div className="paperRight">
                <div className="paperLeft">
                  <div style={{ margin: "14px" }}>
                    <h3>Diskon 1%</h3>
                    <p>Berlaku untuk anda yang berumur antara 30 - 39 Tahun!</p>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
        </Box>
      </Container>
    </div>
  );
};

export default DiscountSection;
