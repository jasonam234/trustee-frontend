import { useEffect, useState } from "react";
import { Image } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  Row,
  Container,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardText,
} from "reactstrap";
import { getDataNoToken } from "../../../api/EndPointAuth";
import { cmsActions } from "../../../store/cms";

const UserReviewSection = () => {
  let dispatch = useDispatch();
  const reviews = useSelector((state) => {
    return state.cms.reviews;
  });
  useEffect(() => {
    getDataNoToken(`/admin/get/cms/review`).then((res) => {
      console.log(res);
      dispatch(cmsActions.allReview(res.data.data));
    });
  }, [dispatch]);

  return (
    <div>
      <Container fluid className="bg-identity2 p-5">
        <div style={{ textAlign: "center" }}>
          <h3 className="fw-bold text-white">Ulasan Konsumen</h3>
          <br />
        </div>
        {reviews.map((value, index) => {
          return (
            <Row>
              <Col key={index}>
                <Card className="m-2">
                  <CardBody>
                    <Row>
                      <Col lg={2} className="float-end p-1 pr-1">
                        <img
                          src={`http://localhost:8081/api/admin/cms/image/${value.profile_picture}`}
                          alt=""
                          style={{ width: "150px", borderRadius:"50%", marginLeft:"10%" }}
                        />
                      </Col>
                      <Col lg={10}>
                        <CardTitle>
                          <h4 className="text-secondary">
                            {value.reviewer_name}
                          </h4>
                        </CardTitle>
                        <CardText className="text-secondary">
                          {value.review_content}..
                        </CardText>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          );
        })}
      </Container>
    </div>
  );
};

export default UserReviewSection;
