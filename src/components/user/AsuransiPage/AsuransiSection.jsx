import { useParams } from "react-router-dom";
import { Col, Row } from "react-bootstrap";
import { useEffect, useRef, useState } from "react";
import { getDataNoToken } from "../../../api/EndPointAuth";
import LoadingSpin from "../../loader/LoadingSpin";

const AsuransiSection = () => {
  const [asuransi, setAsuransi] = useState([]);
  let param = useParams();
  let id = param.premi_id;
  const [isLoading, setIsLoading] = useState(false);

  useEffect( async() => {
    setIsLoading(true)
    const getAsuransi = async () => {
      let res = await getDataNoToken(`/premi/get/jenis/${id}`);
      setAsuransi(res.data.data)
    };
    await getAsuransi();
    setIsLoading(false);
  },[id,setAsuransi]);

  if (isLoading === true) {
    return <LoadingSpin/>
  }
  return (
    <div>
      <Row className="rowCenter">
        <h4 >{asuransi.jenis_name}</h4>
        <hr className="garisPembatas" />
        <Col>
          <img
            src="https://cdn.medcom.id/dynamic/content/2018/11/06/950021/yc6XlZufH7.jpg?w=700"
            alt=""
            className="imagePremi"
          />
        </Col>
        <Col>
          <p className="textTentangAsuransi">
            {asuransi.detail_jenis}
          </p>
        </Col>
        <br />
      </Row>
    </div>
  );
};

export default AsuransiSection;
