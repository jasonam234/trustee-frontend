import { CardBody, CardFooter, CardHeader, CardText } from "reactstrap";
import { Row, Col, Card, Button } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { getDataNoToken } from "../../../api/EndPointAuth";
import LoadingSpin from "../../loader/LoadingSpin";

const PaketSection = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  let param = useParams();
  let premi_id = param.premi_id;
  const [isLoading, setIsLoading] = useState(true);

  const [paket, setPaket] = useState([]);
  useEffect(async () => {
    setIsLoading(true);
    await getDataNoToken(`/premi/get/paket/distinct/${premi_id}`).then(
      (res) => {
        let data = res.data.data;
        setPaket(data);
      }
    );
    setIsLoading(false);
  }, [premi_id, setPaket]);
  if (isLoading === true) {
    return <LoadingSpin />;
  }
  const beliPremi = (id) => {
    navigate(`/asuransi/${premi_id}/` + id);
  };
  console.log(paket);
  return (
    <div>
      <Row className="rowCenter">
        <h4>Paket Premi </h4>
        <hr className="garisPembatas" />
      </Row>
      <Row className="rowCenter">
        {paket.map((value, index) => {
          return (
            <>
              <Col>
                <Card className="shadow p-3 mb-5 rounded cardPaket">
                  <div
                    className="overflow-auto h-50"
                  >
                    <CardBody>
                      <CardHeader>Paket Premi {value.paket_name}</CardHeader>
                      <CardText>{value.detail_paket}</CardText>
                      <CardFooter>
                        <Button
                          className="btnPremi"
                          onClick={() => beliPremi(value.paket_name)}
                        >
                          Pilih
                        </Button>
                      </CardFooter>
                    </CardBody>
                  </div>
                </Card>
              </Col>
            </>
          );
        })}
      </Row>
    </div>
  );
};

export default PaketSection;
