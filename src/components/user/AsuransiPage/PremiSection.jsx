import "../../../assets/user/PremiSection.css";
import { Container, Row } from "react-bootstrap";
import AsuransiSection from "./AsuransiSection";
import PaketSection from "./PaketSection";
import { useState, useEffect } from "react";
import LogoSkeleton from "../../loading/LogoSkeleton";
import DiscountSection from "../LandingPage/DiscountSection";

const PremiSection = () => {
  return (
    <div>
      <Container fluid>
        <Row className="rowCenter">
          <AsuransiSection />
        </Row>
        <br />
        <Row>
          <DiscountSection />
        </Row>
        <br />
        <PaketSection />
      </Container>
    </div>
  );
};

export default PremiSection;
