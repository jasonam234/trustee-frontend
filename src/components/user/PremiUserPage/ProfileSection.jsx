import { useEffect, useState } from "react";
import { Card, Row, Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import { FormGroup, Label } from "reactstrap";
import CustomLoader from "../../loader/CustomLoader"
import { getData } from "../../../api/EndPointAuth";

const ProfileSection = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [user, setUser] = useState({});
  let user_id = localStorage.getItem("user_id");
  const refresh = useSelector((state) => {
    return state.page.refresh;
  });
  useEffect( async () => {
    await getData(`/user/get/user_profile/${user_id}`).then((res) => {
      setUser(res.data.data);
    });
    setIsLoading(false);
  }, [user_id, refresh, setIsLoading]);
  if (isLoading === true) {
    return <CustomLoader/>
  }
  return (
    <div>
      <Row className="rowCenter">
        <h4>Profile User</h4>
        <hr className="garisPembatas" />
      </Row>
      <Card>
        <Row>
          <Col style={{textAlign:"center"}}>
            <div style={{ fontSize: "1.44rem" }}>
              <FormGroup>
                <Label>Nama : {user.full_name}</Label>
                <br />
                <Label>Username : {user.username}</Label>
                <br />
                <Label>No Telepon : {user.phone}</Label>
              </FormGroup>
            </div>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default ProfileSection;
