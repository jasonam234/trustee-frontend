import { useEffect, useState } from "react";
import { Card, Button, Row, Container } from "react-bootstrap";
import { useNavigate } from "react-router";
import {
  getData,
  getDataNoToken,
  postData,
  putData,
} from "../../../api/EndPointAuth";
import ReactPaginate from "react-paginate";
import "../../../assets/user/ListPremiSection.css";
import LogoSkeleton from "../../loading/LogoSkeleton";
import swal from "sweetalert";
import CustomLoader from "../../loader/CustomLoader";
import axios from "axios";

const PER_PAGE = 8;
const ListPremiSection = () => {
  const [isLoading, setIsLoading] = useState(true);
  let navigate = useNavigate();
  let user_id = localStorage.getItem("user_id");
  const [currentPage, setCurrentPage] = useState(0);
  const [data, setData] = useState([]);
  const [perPage] = useState(8);
  const [pageCount, setPageCount] = useState(0);

  const getPremiUser = async () => {
    const res = await getData(`/premi/get/premi/user/${user_id}`);
    const data = res.data.data;
    setData(data);
    setPageCount(Math.ceil(data.length / perPage));
  };

  useEffect( async() => {
    const getItem = async () => {
      await getPremiUser();
    };
    await getItem();
    setIsLoading(false)
  }, isLoading);
  const handlePageClick = ({ selected: selectedPage }) => {
    setCurrentPage(selectedPage);
  };
  const paymentHandler = (id) => {
    getData(`/user/get/payment/${id}`).then((res) => {
      let token = res.data.data;
      console.log(token);
      window.snap.pay(token, {
        onSuccess: function (result) {
          console.log("success");
          alert(`success`);

          navigate({
            pathname: "/user/premi",
          });
          window.location.reload();
        },
        onPending: function (result) {
          console.log("pending");

          navigate({
            pathname: "/user/premi",
          });
          swal("Berhasil","Pembayaran Berhasil","success");
        },
        onError: function (result) {
          console.log("error");
          navigate({
            pathname: "/user/premi",
          });
        },
        onClose: function () {
          console.log(
            "customer closed the popup without finishing the payment"
          );
        },
      });
    });
  };
  const klaimHandler = (premi_id) => {
    navigate(`/user/klaim/premi/${premi_id}`);
  };
  const buttonPremi = (status, premi_id) => {
    if (status === "Aktif") {
      return (
        <div>
          <Button onClick={() => klaimHandler(premi_id)}>Klaim</Button>
          <Button
            style={{ marginLeft: "10px" }}
            onClick={() => exportHandler(premi_id)}
          >
            Cetak Invoice
          </Button>
        </div>
      );
    } else if (status === "Pending") {
      return (
        <Button variant="warning" disabled>
          Bayar
        </Button>
      );
    } else if (status === "Menunggu Pembayaran") {
      return (
        <div>
          <Button
            variant="success"
            onClick={() => {
              paymentHandler(premi_id);
            }}
          >
            Bayar
          </Button>
          <Button
            style={{ marginLeft: "10px" }}
            onClick={() => exportHandler(premi_id)}
          >
            Cetak Invoice
          </Button>
        </div>
      );
    } else if (status === "Mengajukan Klaim") {
      return <Button disabled>Klaim</Button>;
    } else if (status === "Tidak Aktif") {
      return <Button disabled>Klaim</Button>;
    } else if (status === "Ditolak") {
      return "";
    }
  };
  const offset = currentPage * PER_PAGE;
  const currentPageData = data.slice(offset, offset + PER_PAGE).map((value) => {
    return (
      <Card
        style={{
          margin: "10px",
          width: "23%",
          maxWidth: "400px",
        }}
      >
        <Card.Header>{value.paket.jenis.jenis_name} </Card.Header>
        <Card.Body style={{maxHeight:"300px"}}>
          Ahli Waris : {value.ahli_waris.full_name} <br />
          Tertanggung : {value.tertanggung.full_name} <br />
          Status : {value.status.status_name}
        </Card.Body>
        <Card.Footer>
          {buttonPremi(value.status.status_name, value.premi_id)}
        </Card.Footer>
      </Card>
    );
  });
  const exportHandler = async (id) => {
    setIsLoading(true)
    let res = await postData(`/premi/get/premi/invoice/${id}`);
    console.log(res);
    let res2 = await getData(`/premi/get/premi/invoice/${id}`);
    console.log(res2);
    setIsLoading(false)
    window.open("http://127.0.0.1:8081/api/premi/get/premi/invoice/" + id);
  };
  if (isLoading === true) {
    return <CustomLoader />;
  }
  return (
    <div>
      <Row className="rowCenter">
        <h4>List Premi User</h4>
        <hr className="garisPembatas" />
      </Row>
      <Row>
        {data.length > 0 ? (
          <>
            {currentPageData}
            <ReactPaginate
              previousLabel={"← Previous"}
              nextLabel={"Next →"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              onPageChange={handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
            />
          </>
        ) : (
          <p>Belum memilih premi</p>
        )}
      </Row>
    </div>
  );
};

export default ListPremiSection;
