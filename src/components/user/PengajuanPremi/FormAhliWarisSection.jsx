import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Card,
  FormGroup,
  Input,
  Label,
  CardBody,
  FormFeedback,
  Col,
} from "reactstrap";
import { userActions } from "../../../store/user";

const FormAhliWarisSection = (props) => {
  let dispatch = useDispatch();
  const [ahliWaris, setAhliWaris] = useState({
    full_name: "",
    birth_date: "",
    relation: {
      relation_id: "",
    },
    ktp: "",
    ktp_image: "",
    phone: "",
  });
  const [image, setImage] = useState();
  const [isCorrect, setIsCorrect] = useState(false);
  const [isCorrect2, setIsCorrect2] = useState(false);
  const [isCorrect3, setIsCorrect3] = useState(false);
  const formHandler = (evt) => {
    let value = evt.target.value;
    let name = evt.target.name;
    console.log(value);
    if (name === "full_name") {
      let regexName = new RegExp("[a-zA-Z ]+$");
      if (value === "") {
        setIsCorrect(false);
      } else if (!regexName.test(value)) {
        setIsCorrect(true);
      } else {
        setIsCorrect(false)
      }
    } else if (name === "ktp") {
      let regexKtp = new RegExp("[0-9]{16,}$");
      if(value === "") {
        setIsCorrect2(false);
      } else if (!regexKtp.test(value)) {
        setIsCorrect2(true)
      } else {
        setIsCorrect2(false)
      }
    } else if (name === "phone") {
      let regexPhone = new RegExp("[0-9]$");
      if (value === "") {
        setIsCorrect3(false)
      } else if (!regexPhone.test(value)) {
        setIsCorrect3(true)
      } else {
        setIsCorrect3(false)
      }
    }
    setAhliWaris((prevsValue) => {
      return {
        ...prevsValue,
        [evt.target.name]: evt.target.value,
      };
    });
  };
  const hubunganHandler = (evt) => {
    setAhliWaris((prevsValue) => {
      return {
        ...prevsValue,
        relation: {
          ...prevsValue.hubungan,
          relation_id: evt.target.value,
        },
      };
    });
  };
  const imageHandler = async (evt) => {
    const filename = evt.target.value.replace(/^.*\\/, "");
    console.log(filename);
    setAhliWaris((prevsValue) => {
      return {
        ...prevsValue,
        ktp_image: filename,
      };
    });
    setImage(evt.target.files[0]);
  };
  props.imgKtp(image);
  props.ahliWaris(ahliWaris);
  return (
    <Card>
      <CardBody style={{ paddingBottom: "40px" }}>
        <h3 className="textLoginHead">Ahli Waris</h3>
        {/* <Form style={{ textAlign: "left" }}> */}
        <Col style={{ textAlign: "left" }}>
          <FormGroup floating>
            <Input
              type="text"
              name="full_name"
              onChange={formHandler}
              required
              invalid={isCorrect}
            />
            <Label for="userFullname">Nama Lengkap : </Label>
            <FormFeedback>Nama tidak boleh mengandung angka</FormFeedback>
          </FormGroup>
          <FormGroup floating>
            <Input
              name="select"
              type="select"
              onChange={hubunganHandler}
              required
            >
              <option value="" selected>
                Pilih Hubungan
              </option>
              <option value="1">Suami - Istri</option>
              <option value="2">Orang Tua / Saudara Kandung</option>
              <option value="3">Kerabat</option>
            </Input>
            <Label for="userAddress">Hubungan : </Label>
          </FormGroup>
          <FormGroup floating>
            <Input
              type="date"
              name="birth_date"
              onChange={formHandler}
              required
            />
            <Label for="userBD">Tanggal Lahir : </Label>
          </FormGroup>
          <FormGroup floating>
            <Input
              type="text"
              name="ktp"
              onChange={formHandler}
              required
              invalid={isCorrect2}
            />
            <Label for="userNIK">NIK : </Label>
            <FormFeedback>
              Harus berupa angka dan terdiri dari 16 angka
            </FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label for="userKTPIMG">Gambar KTP : </Label>
            <Input
              type="file"
              onChange={imageHandler}
              required
              accept="image/jpg,image/png,image/jpeg"
            />
          </FormGroup>
          <FormGroup floating>
            <Input type="text" name="phone" onChange={formHandler} required invalid={isCorrect3} />
            <Label for="userPhone">No Hp : </Label>
            <FormFeedback>Harus berupa angka</FormFeedback>
          </FormGroup>
        </Col>
        {/* </Form> */}
      </CardBody>
    </Card>
  );
};

export default FormAhliWarisSection;
