import { useState } from "react";
import {
  Card,
  FormGroup,
  Input,
  Label,
  CardBody,
  FormFeedback,
} from "reactstrap";

const FormTertanggungSection = (props) => {
  // tinggal gambar belum
  const [image, setImage] = useState();
  const [isUploaded, setIsUploaded] = useState(false);
  const [tertanggung, setTertanggung] = useState({
    full_name: "",
    birth_date: "",
    ktp: "",
    ktp_image: "",
    phone: "",
    address: "",
    occupation: "",
  });
  const [isCorrect, setIsCorrect] = useState(false);
  const [isCorrect2, setIsCorrect2] = useState(false);
  const [isCorrect3, setIsCorrect3] = useState(false);
  const formHandler = (evt) => {
    let value = evt.target.value;
    let name = evt.target.name;
    console.log(value);
    if (name === "full_name") {
      let regexName = new RegExp("[a-zA-Z ]+$");
      if (value === "") {
        setIsCorrect(false);
      } else if (!regexName.test(value)) {
        console.log(!regexName.test(value));
        setIsCorrect(true);
      }
    } else if (name === "ktp") {
      let regexKtp = new RegExp("[0-9]{16}$");
      if (value === "") {
        setIsCorrect2(false);
      } else if (!regexKtp.test(value)) {
        setIsCorrect2(true);
      } else {
        setIsCorrect2(false);
      }
    } else if (name === "phone") {
      let regexPhone = new RegExp("[0-9]$");
      if (value === "") {
        setIsCorrect3(false);
      } else if (!regexPhone.test(value)) {
        console.log("hai2");
        setIsCorrect3(true);
      } else {
        setIsCorrect3(false);
      }
    }
    setTertanggung((prevsValue) => {
      return {
        ...prevsValue,
        [evt.target.name]: evt.target.value,
      };
    });
  };
  const imageHandler = (evt) => {
    const filename = evt.target.value.replace(/^.*\\/, "");
    setTertanggung((prevsValue) => {
      return {
        ...prevsValue,
        ktp_image: filename,
      };
    });
    setImage(evt.target.files[0]);
    setIsUploaded(true);
  };
  props.imgKtp(image);
  props.tertanggung(tertanggung);
  return (
    <Card>
      <CardBody>
        <h3 className="textLoginHead">Tertanggung</h3>
        <div style={{ textAlign: "left" }}>
          <FormGroup floating>
            <Input
              type="text"
              name="full_name"
              onChange={formHandler}
              required
              invalid={isCorrect}
            />
            <Label for="userFullname">Nama Lengkap : </Label>
            <FormFeedback>Nama tidak boleh mengandung angka</FormFeedback>
          </FormGroup>
          <FormGroup floating>
            <Input
              type="date"
              name="birth_date"
              onChange={formHandler}
              required
            />
            <Label for="userBD">Tanggal Lahir : </Label>
          </FormGroup>
          <FormGroup floating>
            <Input
              type="text"
              name="ktp"
              onChange={formHandler}
              required
              invalid={isCorrect2}
            />
            <Label for="userNIK">NIK : </Label>
            <FormFeedback>
              Harus berupa angka dan terdiri dari 16 angka
            </FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label for="userKTPIMG">Gambar KTP : </Label>
            <Input
              type="file"
              onChange={imageHandler}
              required
              accept="image/jpg,image/png,image/jpeg"
            />
          </FormGroup>
          <FormGroup floating>
            <Input
              required
              type="text"
              name="phone"
              onChange={formHandler}
              invalid={isCorrect3}
            />
            <Label for="userPhone">No Hp : </Label>
            <FormFeedback>Harus berupa angka</FormFeedback>
          </FormGroup>
          <FormGroup floating>
            <Input
              type="text"
              name="occupation"
              onChange={formHandler}
              required
            />
            <Label for="userOccupation">Pekerjaan : </Label>
          </FormGroup>
          <FormGroup floating>
            <Input
              type="textarea"
              name="address"
              onChange={formHandler}
              required
            />
            <Label for="userAddress">Alamat : </Label>
          </FormGroup>
        </div>
        {/* </Form> */}
      </CardBody>
    </Card>
  );
};

export default FormTertanggungSection;
