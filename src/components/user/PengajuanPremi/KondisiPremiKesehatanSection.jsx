import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { Form, Label, Input, FormGroup, Col, Row } from "reactstrap";
import { getData } from "../../../api/EndPointAuth";
import "../../../assets/user/KondisiPremiKesehatanSection.css";
import LoadingSpin from "../../loader/LoadingSpin";

const KondisiPremiKesehatanSection = (props) => {
  const [penyakit, setPenyakit] = useState([]);
  const [kebiasaan, setKebiasaan] = useState([]);
  let param = useParams();
  let premi_id = param.premi_id;
  let refresh = useSelector((state) => {
    return state.page.refresh;
  });
  const getDataForKesehatan = async () => {
    let res = await getData(`/admin/get/kebiasaan`);
    setKebiasaan(res.data.data);
    let res2 = await getData(`/admin/get/penyakit`);
    setPenyakit(res2.data.data);
  };
  const [isLoading, setIsLoading] = useState(true);
  const [syarat, setSyarat] = useState([]);

  useEffect( async() => {
    // console.log("hai");
    await getDataForKesehatan();
    setIsLoading(false)
  }, [setPenyakit, penyakit, kebiasaan, setKebiasaan, refresh, syarat]);

  if (isLoading === true) {
    return <LoadingSpin/>
  }
  const penyakitOkHandler = (evt) => {
    let id = evt.currentTarget.value;
    let search = penyakit.find((el) => el.penyakit_id == id);
    let found = syarat.some((el) => el.penyakit_id == id);

    if (!found) {
      setSyarat((prevsValue) => {
        prevsValue.push(search);
        return prevsValue;
      });
    }
  };

  const penyakitXHandler = (evt) => {
    let id = evt.currentTarget.value;
    let found = syarat.some((el) => el.penyakit_id == id);

    if (found) {
      setSyarat((prevsValue) => {
        prevsValue.forEach((el, i) => {
          if (el.penyakit_id == id) {
            prevsValue.splice(i, 1);
          }
        });
        return prevsValue;
      });
    }
  };

  const kebiasaanOkHandler = (evt) => {
    let id = evt.currentTarget.value;
    let search = kebiasaan.find((el) => el.kebiasaan_id == id);
    let found = syarat.some((el) => el.kebiasaan_id == id);

    if (!found) {
      setSyarat((prevsValue) => {
        prevsValue.push(search);
        return prevsValue;
      });
    }
  };

  const kebiasaanXHandler = (evt) => {
    let id = evt.currentTarget.value;
    let found = syarat.some((el) => el.kebiasaan_id == id);

    if (found) {
      setSyarat((prevsValue) => {
        prevsValue.forEach((el, i) => {
          if (el.penyakit_id == id) {
            prevsValue.splice(i, 1);
          }
        });
        return prevsValue;
      });
    }
  }
  props.syarat(syarat);
  return (
    <div style={{ textAlign: "justify", paddingLeft: "10%" }}>
      <br />
      <Row>
        <Col>
          <h5 className="textCenterKondisi">Penyakit Tertanggung</h5>
          <hr className="garisPembatasKondisi" />
          <h6>Apakah anda memiliki riwayat : </h6>
          <br />
          {penyakit.map((value, index) => {
            return (
              <Form>
                <FormGroup tag="fieldset">
                  <FormGroup>
                    <Label>
                      {index + 1}. {value.penyakit_name}
                    </Label>
                    <br />
                    <Input
                      name={value.penyakit_name}
                      type="radio"
                      value={value.penyakit_id}
                      onChange={penyakitOkHandler}
                    />{" "}
                    <Label className="marginRadio" check>
                      Ya
                    </Label>
                    <Input
                      name={value.penyakit_name}
                      type="radio"
                      value={value.penyakit_id}
                      onChange={penyakitXHandler}
                    />{" "}
                    <Label check>Tidak</Label>
                  </FormGroup>
                </FormGroup>
              </Form>
            );
          })}
        </Col>
        <Col>
          <Form>
            <h5 className="textCenterKondisi">Kebiasaan Tertanggung</h5>
            <hr className="garisPembatasKondisi" />
            <h6>Apakah anda memiliki kebiasaan :</h6>
            <br />
            {kebiasaan.map((value, index) => {
              return (
                <Form>
                  <FormGroup tag="fieldset" >
                    <FormGroup>
                      <Label>
                        {index + 1}. {value.kebiasaan_name}
                      </Label>
                      <br />
                      <Input
                        name={value.kebiasaan_name}
                        type="radio"
                        value={value.kebiasaan_id}
                        onChange={kebiasaanOkHandler}
                      />{" "}
                      <Label className="marginRadio" check>
                        Ya
                      </Label>
                      <Input
                        name={value.kebiasaan_name}
                        type="radio"
                        value={value.kebiasaan_id}
                        onChange={kebiasaanXHandler}
                      />{" "}
                      <Label check>Tidak</Label>
                    </FormGroup>
                  </FormGroup>
                </Form>
              );
            })}
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default KondisiPremiKesehatanSection;
