import { useState } from "react";
import { Row, Button, Container, Col, Form } from "reactstrap";
import { useNavigate, useParams, useLocation } from "react-router-dom";
import FormTertanggungSection from "./FormTertanggungSection";
import FormAhliWarisSection from "./FormAhliWarisSection";
import { useDispatch } from "react-redux";
import { userActions } from "../../../store/user";
import KondisiPremiKesehatanSection from "./KondisiPremiKesehatanSection";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import {  postData, postImage } from "../../../api/EndPointAuth";
import { pageActions } from "../../../store/page";
import LoadingSpin from "../../loader/LoadingSpin";

const FormPengajuan = () => {
  let navigate = useNavigate();
  let location = useLocation();
  let dispatch = useDispatch();
  let param = useParams();
  let id = param.premi_id;
  let paket_id = location.state.paket_id;
  let userId = localStorage.getItem("user_id");
  const [syarat, setSyarat] = useState([]);
  const [ktpAW, setKtpAW] = useState();
  const [ktpT, setKtpT] = useState();
  const user = {
    user: {
      user_id: userId,
    },
  };
  const [isLoading, setIsLoading] = useState("");
  let totalScore;
  const [nilai] = useState(100);
  const [score, setScore] = useState([]);

  const tertanggungHandler = (data) => {
    dispatch(userActions.tertanggung(data));
  };
  const ahliWarisHandler = (data) => {
    dispatch(userActions.ahliWaris(data));
  };
  const tertanggungUser = useSelector((state) => {
    return state.user.tertanggung;
  });
  const ahliWarisUser = useSelector((state) => {
    return state.user.ahliWaris;
  });

  if (isLoading === true) {
    return <LoadingSpin />;
  }

  const pengajuanHandler = async (evt) => {
    evt.preventDefault();
    if (syarat.length === 0) {
      let obj = {
        user: user.user,
        tertanggung: tertanggungUser,
        ahli_waris: ahliWarisUser,
        status: {
          status_id: 1,
        },
        nilai: nilai,
        paket: {
          paket_id: paket_id,
        },
      };
      setIsLoading(true);
      postData(`/premi/post/premi`, obj)
      .then((res) => {
        const ktpAhliWaris = new FormData();
          ktpAhliWaris.append("image", ktpAW);
          postImage(`/user/post/ktp_image`, ktpAhliWaris)
            .then((respone) => {
              const ktpTertanggung = new FormData();
              ktpTertanggung.append("image", ktpT);
              postImage(`/user/post/ktp_image`, ktpTertanggung)
                .then((res) => {
                  setIsLoading(false);
                  swal("Success!", "Pengajuan Premi Berhasil!", "success");
                  dispatch(pageActions.refresh(1));
                  navigate("/");
                })
                .catch((err) => {
                  swal(
                    "Gagal!",
                    "Pengajuan Premi Gagal! Pastikan KTP Tertanggung diisi",
                    "error"
                  );
                });
            })
            .catch((err) => {
              swal(
                "Gagal!",
                "Pengajuan Premi Gagal! Pastikan KTP Ahli waris diisi",
                "error"
              );
            });
        })
        .catch((err) => {
          console.log(err);
          swal(
            "Gagal!",
            "Pengajuan Premi Gagal! Isi data dengan benar",
            "error"
          );
        });
    } else {
      const getScoring = async () => {
        let arrScore = [];
        let loopSyarat = syarat.forEach((el, i) => {
          arrScore.push(el.value);
        });
        totalScore = await arrScore.reduce((prevs, current) => {
          return prevs + current;
        });
      };
      await getScoring();
      let nilaiUser = nilai - totalScore;
      let obj = {
        user: user.user,
        tertanggung: tertanggungUser,
        ahli_waris: ahliWarisUser,
        status: {
          status_id: 1,
        },
        nilai: nilaiUser,
        paket: {
          paket_id: paket_id,
        },
      };
      setIsLoading(true);
      await postData(`/premi/post/premi`, obj)
        .then((res) => {
          const ktpAhliWaris = new FormData();
          ktpAhliWaris.append("image", ktpAW);
          postImage(`/user/post/ktp_image`, ktpAhliWaris)
            .then((res) => {
              const ktpTertanggung = new FormData();
              ktpTertanggung.append("image", ktpT);
              postImage(`/user/post/ktp_image`, ktpTertanggung)
                .then((res) => {
                  setIsLoading(false);
                  swal("Success!", "Pengajuan Premi Berhasil!", "success");
                  dispatch(pageActions.refresh(1));
                  navigate("/");
                })
                .catch((err) => {
                  swal(
                    "Gagal!",
                    "Pengajuan Premi Gagal! Pastikan KTP Tertanggung diisi",
                    "error"
                  );
                });
            })
            .catch((err) => {
              swal(
                "Gagal!",
                "Pengajuan Premi Gagal! Pastikan KTP Ahli waris diisi",
                "error"
              );
            });
        })
        .catch((err) => {
          swal(
            "Gagal!",
            "Pengajuan Premi Gagal! Isi data dengan benar",
            "error"
          );
        });
    }
  };
  return (
    <div>
      <Container fluid>
        <br />
        <Form onSubmit={pengajuanHandler}>
          <Row className="rowCenter">
            <Col>
              <FormTertanggungSection
                imgKtp={setKtpT}
                tertanggung={tertanggungHandler}
              />
            </Col>
            <Col>
              <FormAhliWarisSection
                imgKtp={setKtpAW}
                ahliWaris={ahliWarisHandler}
              />
            </Col>
          </Row>
          {id == 1 ? (
            <Row>
              <KondisiPremiKesehatanSection syarat={setSyarat} />
            </Row>
          ) : (
            ""
          )}

          <Button
            type="submit"
            style={{ marginLeft: "45%", marginTop: "5%", marginBottom: "5%" }}
          >
            Ajukan
          </Button>
        </Form>
      </Container>
    </div>
  );
};

export default FormPengajuan;
