import { useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { Card, Form, FormGroup, Input, Label, Button } from "reactstrap";
import swal from "sweetalert";
import { postImage, putData } from "../../../api/EndPointAuth";

const PengajuanKlaimSection = () => {
  const [FileImg, setFileImg] = useState("");
  const [image, setImage] = useState();
  let navigate = useNavigate();
  let param = useParams();
  let id = param.premi_id;
  const [klaim, setKlaim] = useState({
    premi_id: id,
    claim_image : "",
    status: {
      status_id: 5,
    },
  });

  const imageHandler = (evt) => {
    const filename = evt.target.value.replace(/^.*\\/, "");
    console.log(filename);
    setKlaim((prevsValue) => {
      return {
        ...prevsValue,
        claim_image: filename,
      };
    });
    setImage(evt.target.files[0]);
  };
  const klaimHandler = async (evt) => {
    evt.preventDefault();
    const buktiKlaim = new FormData();
    buktiKlaim.append("image", image);
    postImage(`/premi/claim/image`, buktiKlaim)
      .then((res) => {
        console.log(res);
      })
      .then(
        putData(`/premi/put/premi`, klaim).then(navigate(`/user/premi`))
        .then((res) => {
          console.log(res);
          swal("Berhasil!", "Pengajuan Klaim berhasil diajukan", "success");
        }).catch((err) => {
          swal("Gagal!", "Pengajuan Klaim gagal", "error");
        })
      );
  };

  return (
    <div style={{marginTop:"7%",marginBottom:"6%"}}>
      <center>
        <Card style={{ width: "50%", height: "300px" }}>
          <Form onSubmit={klaimHandler}>
            <FormGroup>
              <Label style={{float:"left"}}>Masukan Bukti Pembayaran :</Label>
              <Input type="file" onChange={imageHandler} accept="image/jpg,image/png,image/jpeg" />
            </FormGroup>
            <Button type="submit">Ajukan</Button>
          </Form>
        </Card>
      </center>
    </div>
  );
};

export default PengajuanKlaimSection;
