import axios from "axios";
import { useEffect, useState } from "react";
import { Navbar, Nav, Container, Dropdown } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { getData, getDataNoToken } from "../../api/EndPointAuth";
import "../../assets/user/NavbarPage.css";
import logo from "../../logo.png";
import { Badge } from "@mui/material";
import { Badge as BadgeBoots } from "react-bootstrap";
import { pageActions } from "../../store/page";

const NavbarPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const isAuth = useSelector((state) => {
    return state.auth.isAdmin;
  });
  const [role, setRole] = useState("");
  const [jenis, setJenis] = useState([]);
  const user_id = localStorage.getItem("user_id");
  // const [count, setCount] = useState(0);
  let roleUser = localStorage.getItem("role");
  const count = useSelector((state) => {
    return state.page.countPremi;
  });
  const refresh = useSelector((state) => {
    return state.page.refresh;
  });
  console.log(refresh);
  useEffect(() => {
    if (roleUser !== null) {
      setRole(roleUser);
    } else {
      setRole("");
    }
    getDataNoToken(`/premi/get/jenis`)
      .then((res) => {
        let data = res.data.data;
        setJenis(data);
        console.log(data);
      })
      .then(
        getData(`/premi/get/premi/notification/${user_id}`).then((res) => {
          dispatch(pageActions.countPremi(res.data.data));
        })
      );
    console.log("refresh navbar");
  }, [setRole, setJenis, refresh]);
  const asuransiPage = (id) => {
    navigate(`/asuransi/` + id);
    dispatch(pageActions.refresh(1));
  };
  console.log(jenis);

  const logoutHandler = () => {
    navigate("/");
    localStorage.clear();
    // window.location.reload();
  };

  return (
    <>
      <Navbar
        expand="lg"
        className="textHeaderNav"
        // style={{ position: "static" }}
        fixed="top"
      >
        <Container fluid>
          <Navbar.Brand className="textNavBrand" as={Link} to="/">
            <img width="100px" src={logo} alt="logo" />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
              <Nav.Link as={Link} to="/about" className="textNavBrand">
                Tentang
              </Nav.Link>
              <Dropdown>
                <Dropdown.Toggle as={Nav.Link} className="textNavBrand">
                  Asuransi
                  <Dropdown.Menu style={{ width: "200px" }}>
                    {jenis.map((value) => {
                      return (
                        <Dropdown.Item
                          onClick={() => asuransiPage(value.jenis_id)}
                        >
                          {value.jenis_name}
                        </Dropdown.Item>
                      );
                    })}
                  </Dropdown.Menu>
                </Dropdown.Toggle>
              </Dropdown>
            </Nav>
            <Nav>
              {role === "user" ? (
                // {isAuth ?(
                <Dropdown>
                  <Dropdown.Toggle
                    as={Nav.Link}
                    style={{ marginRight: "50px" }}
                  >
                    {count > 0 ? (
                      <Badge variant="dot" color="error">
                        <span className="fa fa-user">Anggota</span>
                      </Badge>
                    ) : (
                      <span className="fa fa-user">Anggota</span>
                    )}
                    <Dropdown.Menu style={{ width: "150px" }}>
                      <Dropdown.Item as={Link} to="/user/premi">
                        Profil
                        {count > 0 ? (
                          <BadgeBoots color="primary">{count}</BadgeBoots>
                        ) : (
                          ""
                        )}
                      </Dropdown.Item>
                      <Dropdown.Item onClick={logoutHandler}>
                        Keluar
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown.Toggle>
                </Dropdown>
              ) : (
                <>
                  <Nav.Link as={Link} to="/login">
                    Masuk
                  </Nav.Link>
                  <Nav.Link as={Link} to="/register">
                    Daftar
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default NavbarPage;
