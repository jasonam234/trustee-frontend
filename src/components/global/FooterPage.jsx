import { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { getDataNoToken } from "../../api/EndPointAuth";
import "../../assets/global/Footer.css";
import logo from "../../logo.png";

const FooterPage = () => {
    const [asuransi, setAsuransi] = useState([]);
    useEffect(() => {
        getDataNoToken(`/premi/get/jenis`).then((res) => {
            console.log(res.data.data);
            let data = res.data.data;
            setAsuransi(data);
        });
    }, [setAsuransi]);
    return (
        <div className="bg-identity2">
            <Row>
                <Col lg={6} className="pe-5">
                    <div className="pe-4">
                        <iframe
                            className="w-100 frame-maps "
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.0494715113077!2d106.61418631431056!3d-6.257213663003063!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fc6527701e45%3A0xcae99f5b64059207!2sJl.%20Scientia%20Boulevard%20Blk.%20A%20No.20%2C%20Curug%20Sangereng%2C%20Kec.%20Klp.%20Dua%2C%20Tangerang%2C%20Banten%2015810!5e0!3m2!1sid!2sid!4v1637723111657!5m2!1sid!2sid"
                            allowFullScreen="true"
                            height="200px"
                        ></iframe>
                    </div>
                </Col>
                <Col lg={6} className="p-4">
                    <Row>
                        <Col md={6}>
                            <h5 className="nav-link text-white mt-4 ps-5">Produk</h5>
                            <ul className="text-white">
                                {asuransi.map((value) => {
                                    return (
                                        <li className="list-style-type">
                                            <Link
                                                to={"asuransi/" + value.jenis_id}
                                                className="nav-link text-white"
                                            >
                                                {value.jenis_name}
                                            </Link>
                                        </li>
                                    );
                                })}
                            </ul>
                        </Col>
                        <Col md={6} className="px-3">
                            <img className="mt-4" width="200px" src={logo} alt="logo" />
                            <p className="fs-5 text-white">
                                Trustee hadir untuk memberikan proteksi dalam kehidupan kaum muda
                                dan keluarga anda
                            </p>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <footer className="text-white">
                <p>
                    Copyright &copy; <span className="text-identity">Trustee</span> 2021 All Right
                    Reserved
                </p>
            </footer>
        </div>
    );
};

export default FooterPage;
