import { Col } from "react-bootstrap";
import "../../assets/global/Global.css";

export default function NotFound() {
    return (
        <div>
            <br/>
            <br/>
            <br/>
            <br/>
            <Col lg={5} className="mt-7 p-5 m-auto shadow-lg bg-custom rounded-05 text-center ">
            <h1 className="fs-200 light">404</h1>
            <h3 className="light">oopss..</h3>
            <p className="light">Page Not Found</p>
        </Col>
        </div>
        
    );
}
