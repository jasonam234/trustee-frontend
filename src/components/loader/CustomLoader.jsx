import { Spinner } from "./RotateCustom";

const CustomLoader = () => (
	<div className="m-auto p-5">
		<Spinner />
		<div className="m-auto"><p  className="text-center text-secondary">Mengumpulkan data...</p></div>
	</div>
);
export default CustomLoader