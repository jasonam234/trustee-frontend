import { Spinner } from "./RotateCustom";

const SendLoader = () => (
	<div className="m-auto p-5">
		<Spinner />
		<div className="m-auto"><p  className="text-center text-secondary">Mengirim email ...</p></div>
	</div>
);
export default SendLoader