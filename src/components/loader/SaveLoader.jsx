import { Spinner } from "./RotateCustom";

const SaveLoader = () => (
	<div className="m-auto p-5">
		<Spinner />
		<div className="m-auto"><p  className="text-center text-secondary">Menyimpan data...</p></div>
	</div>
);
export default SaveLoader