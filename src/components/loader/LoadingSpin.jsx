import { Spinner } from "./RotateCustom";

const LoadingSpin = () => {
  return (
    <div className="mt-5 p-5">
      <Spinner />
      <div className="m-auto">
        <p className="text-center text-secondary">Loading...</p>
      </div>
    </div>
  );
};

export default LoadingSpin;
