import styled, { keyframes } from 'styled-components';
export const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;
export const Spinner = styled.div`
	
	animation: ${rotate360} 1s linear infinite;
	transform: translateZ(0);
    margin: 0 auto;
	border-top: 2px solid #e3e3e3;
	border-right: 2px solid #e3e3e3;
	border-bottom: 2px solid #e3e3e3;
	border-left: 4px solid #c4c4c4;
	background: transparent;
	width: 50px;
	height: 50px;
	border-radius: 50%;
`;
