const Status = ({ status }) => {
    const style =
        status.status_id === 1
            ? "bg-secondary box-custom m-auto"
            : status.status_id === 2
            ? "bg-info box-custom m-auto"
            : status.status_id === 3
            ? "bg-success box-custom m-auto"
            : status.status_id === 4
            ? "bg-danger box-custom m-auto"
            : status.status_id === 5
            ? "bg-warning box-custom m-auto"
            : status.status_id === 6
            ? "bg-dark box-custom m-auto"
            : "bg-dark box-custom m-auto";

    return (
        <div className={style}>
            <h5>Status : {status.status_name}</h5>
        </div>
    );
};
export default Status;
