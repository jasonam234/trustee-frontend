import React from "react";
import { Button } from "react-bootstrap";
import FileSaver from "file-saver";
import XLSX from "xlsx";

const ExportCSV = ({ csvData, fileName, wscols }) => {
    const fileType =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const fileExtension = ".xlsx";

    const Heading = [
        {
            tanggal_bayar: "Tanggal",
            jenis_name: "Jenis Asuransi",
            paket_name: "Paket Premi",
            price: "Jumlah (Rp)",
        },
    ];

    const exportToCSV = (csvData, fileName, wscols) => {
        const ws = XLSX.utils.json_to_sheet(Heading, {
            header: ["tanggal_bayar", "jenis_name", "paket_name", "price"],
            skipHeader: true,
            origin: 0, //ok
        });
        ws["!cols"] = wscols;
        XLSX.utils.sheet_add_json(ws, csvData, {
            header: ["tanggal_bayar", "jenis_name", "paket_name", "price"],
            skipHeader: true,
            origin: -1, //ok
        });
        const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
        const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExtension);
    };

    return (
        <Button
            className="m-2"
            variant="success"
            onClick={(e) => exportToCSV(csvData, fileName, wscols)}
        >
            Export XLSX
        </Button>
    );
};

export default ExportCSV;
