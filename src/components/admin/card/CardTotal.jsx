import { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { Col } from "reactstrap";
import { getCountAllInsurance } from "../../../api/EndPointReport";
import CustomLoader from "../../loader/CustomLoader";

export default function CardTotal() {
    const [allIns, setAllIns] = useState(0);
    const [pending, setPending] = useState(true);
    useEffect(async()=>{
        await getCountAllInsurance()
        .then((res) => {
            setAllIns(res.data.data);
        })
        .catch((err) => {
            console.log(err);
        });
        setPending(false);
    })
    if (pending == true ) {
        return (
                <CustomLoader />
        );
    }
    
    return (
        <Col lg={5} className="mt-2 mb-2 m-auto ">
            <Card className="bg-primary text-center" text="white">
                <Card.Body className="light">
                    <Card.Title>Grand Total</Card.Title>
                    <Card.Text>
                        <h2 className="shadow m-3 p-2 text-center">{allIns > 0 ? allIns : 0}</h2>
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    );
}
