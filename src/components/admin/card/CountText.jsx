import { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { getCountInsuranceType } from "../../../api/EndPointReport";
export default function CountText({ id }) {
    const [count, setCount] = useState([]);
    useEffect(async () => {
        await getCountInsuranceType(id)
            .then((res) => {
                setCount(res.data.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [setCount]);
    return (
        <Card.Text>
            <h2 className="shadow m-3 p-2 text-center">
                <p>{count}</p>
            </h2>
        </Card.Text>
    );
}
