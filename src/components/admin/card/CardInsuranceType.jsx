import { useEffect, useState } from "react";
import { Card, Col } from "react-bootstrap";
import { getAllInsuranceType } from "../../../api/EndPointInsurance";
import CustomLoader from "../../loader/CustomLoader";
import CountText from "./CountText";

export default function CardInsuranceType() {
    const [insuranceType, setInsuranceType] = useState([]);
    const [pending, setPending] = useState(true);
    useEffect(async () => {
        await getAllInsuranceType()
            .then((res) => {
                setInsuranceType(res.data.data);
            })
            .catch((err) => {
                console.log(err);
            });
            setPending(false);
    }, [setInsuranceType]);
    if (pending == true ) {
        return (
                <CustomLoader />
        );
    }
    return insuranceType.map((value) => {
        return (
            <Col lg={3} className="mt-2 mb-2 m-auto ">
                <Card className="bg-info text-center" text="white">
                    <Card.Body className="light">
                        <Card.Title>{value.jenis_name}</Card.Title>
                        <Card.Text>
                            <CountText id={value.jenis_id} />
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        );
    });
}
