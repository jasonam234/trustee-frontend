import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useDispatch, useSelector } from "react-redux";
import { getAllRelation } from "../../../api/EndPointCMS";
import { formActions } from "../../../store/form";
import RelationshipColumns from "../../../components/admin/columns/RelationshipColumns";
import { empty } from "./ListEmpty";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";

const RelationList = () => {
    const [pending, setPending] = useState(true);
    const dispatch = useDispatch();
    const relation = useSelector((state) => state.form.relation);
    useEffect(async() => {
            await getAllRelation()
                .then((res) => {
                    dispatch(formActions.allRelation(res.data.data));
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <DataTable
            columns={RelationshipColumns}
            data={relation}
            progressPending={pending}
            progressComponent={<CustomLoader />}
            noDataComponent={ empty()}
            pagination
            dense
        />
    );
};
export default RelationList;
