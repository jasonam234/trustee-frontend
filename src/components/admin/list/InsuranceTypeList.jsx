import DataTable from "react-data-table-component";
import InsuranceTypeColumns from "../../../components/admin/columns/InsuranceTypeColumns";
import { insuranceActions } from "../../../store/insurance";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { getAllInsuranceType } from "../../../api/EndPointInsurance";
import { empty } from "./ListEmpty";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
const InsuranceTypeList = () => {
    const [pending, setPending] = useState(true);
    const insuranceType = useSelector((state) => state.insurance.insuranceType);
    const dispatch = useDispatch();
    useEffect(async() => {
            await getAllInsuranceType()
                .then((res) => {
                    dispatch(insuranceActions.allInsuranceType(res.data.data));
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <DataTable
            columns={InsuranceTypeColumns}
            data={insuranceType}
            progressPending={pending}
            progressComponent={<CustomLoader />}
            noDataComponent={empty()}
            pagination
            dense
        />
    );
};
export default InsuranceTypeList;
