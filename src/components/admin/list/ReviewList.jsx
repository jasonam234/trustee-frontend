import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { getAllReview } from "../../../api/EndPointCMS";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
import ReviewColumns from "../columns/ReviewColumns";
import { empty } from "./ListEmpty";
const ReviewList = () => {
    const [pending, setPending] = useState(true);
    const [reviews, setReviews] = useState([]);
    useEffect(async() => {
            await getAllReview()
                .then((res) => {
                    setReviews(res.data.data);
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [setReviews]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <DataTable
            columns={ReviewColumns}
            data={reviews}
            progressPending={pending}
            progressComponent={<CustomLoader />}
            noDataComponent={empty() }
            pagination
            dense
        />
    );
};
export default ReviewList;
