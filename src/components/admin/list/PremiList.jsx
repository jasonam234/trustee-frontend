import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getAllPremi } from "../../../api/EndPointPremi";
import { premiActions } from "../../../store/premi";
import CustomLoader from "../../loader/CustomLoader";
import RequestColumns from "../columns/RequestColumns";
import { empty } from "./ListEmpty";

const PremiList = () => {
    const [pending, setPending] = useState(true);
    const premi = useSelector((state) => state.premi.premi);
    const dispatch = useDispatch();
    useEffect(async () => {
        await getAllPremi()
            .then((res) => {
                dispatch(premiActions.allPremi(res.data.data));
            })
            .catch((err) => {
                console.log(err);
            });
        setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <div>
            <DataTable
                columns={RequestColumns}
                data={premi}
                noHeader
                progressPending={pending}
                progressComponent={<CustomLoader />}
                noDataComponent={empty()}
                defaultSortFieldId={2}
                defaultSortAsc={false}
                highlightOnHover
                paginationResetDefaultPage
                pagination
                dense
            />
        </div>
    );
};

export default PremiList;
