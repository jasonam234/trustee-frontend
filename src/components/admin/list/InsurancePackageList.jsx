import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getAllPackagePremi } from "../../../api/EndPointPremi";
import { insuranceActions } from "../../../store/insurance";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
import InsurancePremiColumns from "../columns/InsurancePremiColumns";
import { empty } from "./ListEmpty";

const InsurancePackageList = () => {
    const [pending, setPending] = useState(true);
    const insurancePackage = useSelector((state) => state.insurance.insurancePackage);
    const dispatch = useDispatch();
    useEffect(async() => {
           await getAllPackagePremi()
                .then((res) => {
                    dispatch(insuranceActions.allInsurancePackage(res.data.data));
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <DataTable
            columns={InsurancePremiColumns}
            data={insurancePackage}
            progressPending={pending}
            progressComponent={<CustomLoader />}
            noDataComponent={empty()}
            highlightOnHover
            pagination
            dense
        />
    );
};

export default InsurancePackageList;
