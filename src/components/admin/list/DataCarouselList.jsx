import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useDispatch, useSelector } from "react-redux";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
import CarouselColumns from "../columns/CarouselColumns";
import CarouselList from "./CarouselList";
import { empty } from "./ListEmpty";

const DataCarouselList = () => {
    const dispatch = useDispatch();
    const [pending, setPending] = useState(true);
    useEffect(() => {
            setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <DataTable
            columns={CarouselColumns}
            data={CarouselList}
            progressPending={pending}
            progressComponent={<CustomLoader />}
            noDataComponent={empty()}
            pagination
            dense
        />
    );
};
export default DataCarouselList;
