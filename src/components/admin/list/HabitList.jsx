import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useDispatch, useSelector } from "react-redux";
import { getAllHabit } from "../../../api/EndPointCMS";
import { formActions } from "../../../store/form";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
import HabitColumns from "../columns/HabitColumns";
import { empty } from "./ListEmpty";

const HabitList = () => {
    const dispatch = useDispatch();
    const [pending, setPending] = useState(true);
    const habit = useSelector((state) => state.form.habit);
    useEffect(async() => {
            await getAllHabit()
                .then((res) => {
                    dispatch(formActions.allHabit(res.data.data));
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <DataTable
            columns={HabitColumns}
            data={habit}
            progressPending={pending}
            progressComponent={<CustomLoader />}
            noDataComponent={empty()}
            pagination
            dense
        />
    );
};
export default HabitList;
