import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getAllClaim } from "../../../api/EndPointClaim";
import { premiActions } from "../../../store/premi";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
import RequestColumns from "../columns/RequestColumns";
import { empty } from "./ListEmpty";

const ClaimList = () => {
    const [pending, setPending] = useState(true);
    const claim = useSelector((state) => state.premi.claim);
    const dispatch = useDispatch();
    useEffect(async () => {
            await getAllClaim()
                .then((res) => {
                    dispatch(premiActions.allClaim(res.data.data));
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    return (
        <DataTable
            columns={RequestColumns}
            data={claim}
            progressPending={pending}
            progressComponent={<CustomLoader/>}
            noDataComponent={empty()}
            highlightOnHover
            pagination
            dense
        />
    );
};

export default ClaimList;
