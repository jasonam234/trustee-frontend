import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { useDispatch, useSelector } from "react-redux";
import { getAllDiseases } from "../../../api/EndPointCMS";
import { formActions } from "../../../store/form";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
import DiseasesHistoryColumns from "../columns/DiseasesHistoryColumns";
import { empty } from "./ListEmpty";

const DiseasesHistoryList = () => {
    const [pending, setPending] = useState(true);
    const dispatch = useDispatch();
    const diseases = useSelector((state) => state.form.diseases);
    useEffect(async() => {
            await getAllDiseases()
                .then((res) => {
                    dispatch(formActions.allDiseases(res.data.data));
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [dispatch]);
    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    
    return (
        <DataTable
            columns={DiseasesHistoryColumns}
            data={diseases}
            progressPending={pending}
            progressComponent={<CustomLoader />}
            noDataComponent={ empty()}
            pagination
            dense
        />
    );
};
export default DiseasesHistoryList;
