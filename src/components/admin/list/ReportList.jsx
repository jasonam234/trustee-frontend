import React, { useEffect, useState } from "react";

import { Button } from "react-bootstrap";
import DataTable from "react-data-table-component";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getListReport } from "../../../api/EndPointReport";
import { reportActions } from "../../../store/report";
import ReportColumns from "../columns/ReportColumns";
import ExportCSV from "../../exportReport/ExportCSV";
import ExportReactCSV from "../../exportReport/ExportReactCSV";
import { empty } from "./ListEmpty";
import CustomLoader from "../../loader/CustomLoader";
import GetDataSuccess from "../../loader/GetDataSuccess";
const ReportList = () => {
    const [pending, setPending] = useState(true);
    const report = useSelector((state) => state.report.report);
    const dispatch = useDispatch();
    useEffect(async () => {
         await getListReport()
            .then((res) => {
                dispatch(reportActions.getReport(res.data.data));
            })
            .catch((err) => {
                console.log(err);
            });
            setPending(false);
    }, [dispatch]);

    if (pending===true) {
        return (
            <div className="">
                <CustomLoader />
            </div>
        );
    }
    
    const headers = [
        { label: "Tanggal", key: "tanggal_bayar" },
        { label: "Jenis Asuransi", key: "jenis_name" },
        { label: "Paket Premi", key: "paket_name" },
        { label: "Jumlah (Rp)", key: "price" },
    ];

    const wscols = [
        { wch: Math.max(...report.map((report) => report.tanggal_bayar.length)) },
        { wch: Math.max(...report.map((report) => report.jenis_name.length)) },
        { wch: Math.max(...report.map((report) => report.paket_name.length)) },
        { wch: Math.max(...report.map((report) => report.price.length)) },
    ];

    return (
        <>
            <div>
                <div className="float-end">
                    <ExportCSV csvData={report} fileName="Report" wscols={wscols} />
                    <ExportReactCSV csvHeaders={headers} csvData={report} fileName="Report.csv" />
                </div>
            </div>

            <DataTable
                columns={ReportColumns}
                data={report}
                defaultSortFieldId={2}
                defaultSortAsc={false}
                nprogressPending={pending}
                progressComponent={<CustomLoader />}
                noDataComponent={empty() }
                pagination
                dense
            />
        </>
    );
};
export default ReportList;
