import { AiFillEdit } from "react-icons/ai";
import { Link } from "react-router-dom";
import { source_image } from "../../../api/EndPointCMS";

export default [
    {
        id: 1,
        carousel: "carousel_one",
        img: (
            <>
                <img
                    src={source_image+"carousel_one.jpg"}
                    class="img-fluid m-2"
                    alt="..."
                ></img>
            </>
        ),
        aksi: (
            <>
                <Link to="/konten/carousel/carousel_one.jpg">
                   <AiFillEdit style={{color: "green"}} size={22} />
                </Link>
            </>
        ),
    },
    {
        id: 2,
        carousel: "carousel_two",
        img: (
            <>
                <img
                    src={source_image+"carousel_two.jpg"}
                    class="img-fluid m-2"
                    alt="..."
                ></img>
            </>
        ),
        aksi: (
            <>
                <Link to="/konten/carousel/carousel_two.jpg">
                   <AiFillEdit style={{color: "green"}} size={22} />
                </Link>
                {/* <AiFillContainer /> */}
            </>
        ),
    },
    {
        id: 3,
        carousel: "carousel_three",
        img: (
            <>
                <img
                    src={source_image+"carousel_three.jpg"}
                    class="img-fluid m-2"
                    alt="..."
                ></img>
            </>
        ),
        aksi: (
            <>
                <Link to="/konten/carousel/carousel_three.jpg">
                   <AiFillEdit style={{color: "green"}} size={22} />
                </Link>
                {/* <AiFillContainer /> */}
            </>
        ),
    },
];
