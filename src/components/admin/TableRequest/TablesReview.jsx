import { Card, Table } from "react-bootstrap";
import { AiFillEdit, AiFillContainer } from "react-icons/ai"; 

const TablesReview = () => {
    return (
        <Card className="table-responsive p-3">
            <Table className="table-hover table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Deskripsi</th>
                        <th>aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td><AiFillEdit /> <AiFillContainer/></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td><AiFillEdit /> <AiFillContainer/></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>
                       <AiFillEdit style={green} size={22} /> <AiFillContainer/>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Card>
    );
};

export default TablesReview;
