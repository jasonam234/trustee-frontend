import { AiFillEdit, AiOutlineUpload } from "react-icons/ai";
import { Link } from "react-router-dom";
import { FaCloudUploadAlt } from "react-icons/fa";
import { source_image } from "../../../api/EndPointCMS";

export default [
    {
        name: "No",
        selector: (row, index) => index + 1,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "80px",
    },
    {
        name: "Nama",
        selector: (row) => row.reviewer_name,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "200px",
    },
    {
        name: "Deskripsi",
        selector: (row) => row.review_content,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "400px",
        wrap: true,
    },
    {
        name: "Gambar",
        selector: (row) => {
            return (
                <>
                    <img
                        src={source_image+row.profile_picture}
                        class="img-fluid m-2 w-80"
                        alt="..."
                    ></img>
                </>
            );
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "300px",
        wrap: true,
    },
    {
        name: "Aksi",
        selector: (row) => {
            const green = { color: "green" };
            return (
                <>
                    <Link to={"/konten/review/" + row.cms_id}>
                        <AiFillEdit style={green} size={22} />
                    </Link>
                    <Link to={"/konten/review-gambar/" + row.profile_picture}>
                        <FaCloudUploadAlt size={22} />
                    </Link>
                </>
            );
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "80px",
    },
];
