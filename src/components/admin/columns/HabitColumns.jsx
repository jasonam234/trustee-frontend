import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import { deleteHabit } from "../../../api/EndPointCMS";

export default [
    {
        name: "No",
        selector: (row, index) => index + 1,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "80px",
    },
    {
        name: "Jenis Kebiasaan",
        selector: row => row.kebiasaan_name,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
    },
    {
        name: "Bobot Penilaian",
        selector: row => row.value,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
    },
    {
        name: "Aksi",
        selector: (row) => {
            const green = { color: "green" };
            const red = { color: "red" };
            const deleteHandler = () => {
                swal("", "Apakah yakin menghapus item ini", "info", {
                    buttons: {
                        cancel: "Batalkan",
                        catch: {
                            text: "Hapus",
                            value: "catch",
                        },
                    },
                }).then((value) => {
                    switch (value) {
                        case "catch":
                            if (row.kebiasaan_id === "") {
                                swal("", "Gagal hapus item", "error");
                            } else {
                                deleteHabit(row.kebiasaan_id)
                                    .then(async (res) => {
                                        console.log(res);
                                        await swal("", "Berhasil Hapus Item", "success");
                                        window.location.reload();
                                    })
                                    .catch((err) => {
                                        swal("", "Gagal hapus item", "error");
                                    });
                            }
                            break;
                        default:
                    }
                });
            };
            return (
                <>
                    <Link to={"/kebiasaan/" + row.kebiasaan_id}>
                        <AiFillEdit style={green} size={22} />
                    </Link>
                    <AiFillDelete style={red} size={22} onClick={deleteHandler} />
                </>
            );
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "80px",
    },
];
