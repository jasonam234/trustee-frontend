import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import { deletePackagePremi } from "../../../api/EndPointPremi";

export default [
    {
        name: "No",
        selector: (row, index) => index + 1,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "80px",
    },
    {
        name: "Jenis Asuransi",
        selector: (row) => row.jenis.jenis_name,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "300px",
    },
    {
        name: "Paket Asuransi",
        selector: "paket_name",
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "300px",
    },
    {
        name: "Periode",
        selector: "period",
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "100px",
    },
    {
        name: "Harga",
        selector: (row) => {
            const formatter = new Intl.NumberFormat("id-Id", {
                style: "currency",
                currency: "IDR",
            });

            return formatter.format(row.price);
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
    },
    {
        name: "Aksi",
        selector: (row) => {
            const green = { color: "green" };
            const red = { color: "red" };
            const deleteHandler = () => {
                swal("", "Apakah yakin menghapus item ini", "info", {
                    buttons: {
                        cancel: "Batalkan",
                        catch: {
                            text: "Hapus",
                            value: "catch",
                        },
                    },
                }).then((value) => {
                    switch (value) {
                        case "catch":
                            if (row.relation_id === "") {
                                swal("", "Gagal hapus item", "error");
                            } else {
                                deletePackagePremi(row.paket_id)
                                    .then(async (res) => {
                                        console.log(res);
                                        await swal("", "Berhasil Hapus Item", "success");
                                        window.location.reload();
                                    })
                                    .catch((err) => {
                                        swal("", "Gagal hapus item", "error");
                                    });
                            }

                            break;

                        default:
                    }
                });
            };
            return (
                <>
                    <Link to={"/paket_premi/" + row.paket_id}>
                        <AiFillEdit style={green} size={22} />
                    </Link>
                    <AiFillDelete style={red} size={22} onClick={deleteHandler} />
                </>
            );
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "100px",
    },
];
