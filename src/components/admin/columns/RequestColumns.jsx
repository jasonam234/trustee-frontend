import { AiFillEdit } from "react-icons/ai";
import { Link } from "react-router-dom";

export default [
    {
        name: "No",
        selector: (row, index) => index + 1,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "100px",
    },
    {
        name: "Tanggal Pengajuan",
        selector: (row) => {
            return row.created_at
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        wrap: true,
    },
    {
        name: "Jenis",
        selector: (row) => row.paket.jenis.jenis_name,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        wrap: true,
    },
    {
        name: "Paket",
        selector: (row) => row.paket.paket_name,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "80px",
    },
    {
        name: "Tetanggung",
        selector: (row) => row.tertanggung.full_name,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        wrap: true,
    },
    {
        name: "Ahli Waris",
        selector: (row) => row.ahli_waris.full_name,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        wrap: true,
    },
    {
        name: "Status",
        selector: (row) => {
            const status = row.status.status_id;

            return status === 1 ? (
                <span className="badge rounded-pill bg-secondary p-2 m-2 fs-12 custom-width">
                    {row.status.status_name}
                </span>
            ) : status === 2 ? (
                <span className="badge rounded-pill bg-info p-2 m-2 fs-12 custom-width">
                    {row.status.status_name}
                </span>
            ) : status === 3 ? (
                <span className="badge rounded-pill bg-success p-2 m-2 fs-12 custom-width">
                    {row.status.status_name}
                </span>
            ) : status === 4 ? (
                <span className="badge rounded-pill bg-danger p-2 m-2 fs-12 custom-width">
                    {row.status.status_name}
                </span>
            ) : status === 5 ? (
                <span className="badge rounded-pill bg-warning p-2 m-2 fs-12 custom-width">
                    {row.status.status_name}
                </span>
            ) : status === 6 ? (
                <span className="badge rounded-pill bg-dark p-2 m-2 fs-12 custom-width">
                    {row.status.status_name}
                </span>
            ) : (
                <span className="badge rounded-pill bg-dark p-2 m-2 fs-12 custom-width">
                    {row.status.status_name}
                </span>
            );
        },
        sortable: true,
        center: true,
        style: {
            width: "100% ",
            borderRight: "solid 2px #e7e7e7",
        },
        width: "200px",
        wrap: true,
    },
    {
        name: "Aksi",
        selector: (row) => {
            const green = { color: "green" };
            return (
                <>
                    <Link to={"/premi/" + row.premi_id}>
                        <AiFillEdit style={green} size={22} />
                    </Link>
                </>
            );
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
        width: "70px",
    },
];
