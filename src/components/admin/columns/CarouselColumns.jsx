export default [       {
    name: "No",
    selector: "id",
    sortable: true,
    style: {
        borderRight: "solid 2px #e7e7e7",
    },
    width:"80px"
},
{
    name: "Carousel",
    selector: "carousel",
    sortable: true,
    style: {
        borderRight: "solid 2px #e7e7e7",
    },
    width:"200px"
    
},
{
    name: "Gambar",
    selector: "img",
    sortable: true,
    style: {
        borderRight: "solid 2px #e7e7e7",
    },
},
{
    name: "Aksi",
    selector: "aksi",
    sortable: true,
    style: {
        borderRight: "solid 2px #e7e7e7",
    },
    width:"80px"
},]