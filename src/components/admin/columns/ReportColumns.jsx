
import Moment from 'react-moment';
export default [
    {
        name: "No",
        selector: (row, index) => index + 1,
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
    },
    {
        name: "Tanggal",
        selector: row=>{
            return row.tanggal_bayar
        },
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
    },
    {
        name: "Jenis Asuransi",
        selector: "jenis_name",
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
    },
    {
        name: "Paket Premi",
        selector: "paket_name",
        sortable: true,
        style: {
            borderRight: "solid 2px #e7e7e7",
        },
    },
    {
        name: "Jumlah (Rp)",
        selector: row=>{
            const formatter = new Intl.NumberFormat("id-Id", {
                style: "currency",
                currency: "IDR",
            });

            return formatter.format(row.price);
        },
        sortable: true,
        style: {
            textAlign:"right",
            borderRight: "solid 2px #e7e7e7",
        },
    },
];
