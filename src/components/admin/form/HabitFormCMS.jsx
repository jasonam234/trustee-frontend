import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import {  getHabitById, postHabit, putHabit } from "../../../api/EndPointCMS";
import CustomLoader from "../../loader/CustomLoader";
import SaveLoader from "../../loader/SaveLoader";
const HabitFormCMS = () => {
    const navigate = useNavigate();
    const [habit, setHabit] = useState({
        kebiasaan_id: 0,
        kebiasaan_name: "",
        value: 0,
    });

    const location = useLocation();
    const [, , id] = location.pathname.split("/");
    const [save, setSave] = useState(false);
    const [pending, setPending] = useState(true);

    useEffect(async() => {
            await getHabitById(id)
                .then((res) => {
                    setHabit(res.data.data);
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [setHabit]);
    if (pending == true && id != "tambah") {
        return (
            <div className="mt-7">
                <CustomLoader />
            </div>
        );
    }
    if (save == true) {
        return (
            <div className="mt-7">
                <SaveLoader />
            </div>
        );
    }
    const formHandler = (evt) => {
        setHabit((prevsValue) => {
            return { ...prevsValue, [evt.target.name]: evt.target.value };
        });
    };
    const formEditHandler = (evt) => {
        setHabit((prevsValue) => {
            return {
                ...prevsValue,
                kebiasaan_id: id,
                [evt.target.name]: evt.target.value,
            };
        });
    };

    const addHandler = (evt) => {
        evt.preventDefault();
        if (habit.kebiasaan_name === "") {
            swal("", "Jenis Kebiasaan tidak boleh kosong", "warning");
        } else if (habit.value === "" || habit.value === 0) {
            swal("", "Bobot tidak boleh kosong atau bernilai 0", "warning");
        } else if (habit.kebiasaan_name === "" || habit.value === "") {
            swal("", "Data tidak boleh kosong", "warning");
        } else if (habit.value <= 0) {
            swal("", "Bobot nilai tidak boleh kurang dari 1", "warning");
        } else if (habit.value > 30) {
            swal("", "Bobot nilai tidak boleh lebih dari dari 30", "warning");
        } else {
            setSave(true)
            postHabit(habit)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data Berhasil disimpan", "success");
                    navigate("/kebiasaan");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    const editHandler = (evt) => {
        evt.preventDefault();
        if (habit.kebiasaan_name === "") {
            swal("", "Jenis Kebiasaan tidak boleh kosong", "warning");
        } else if (habit.value === "" || habit.value === 0) {
            swal("", "Bobot tidak boleh kosong atau bernilai 0", "warning");
        } else if (habit.kebiasaan_name === "" || habit.value === "") {
            swal("", "Data tidak boleh kosong", "warning");
        } else if (habit.value <= 0) {
            swal("", "Bobot nilai tidak boleh kurang dari 1", "warning");
        } else if (habit.value > 30) {
            swal("", "Bobot nilai tidak boleh lebih dari dari 30", "warning");
        } else {
            setSave(true)
            putHabit(habit)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data Berhasil disimpan", "success");
                    navigate("/kebiasaan");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    return (
        <>
         <h5 className="text-secondary">Jenis Kebiasaan</h5>
            <Card>
                <Form className="m-3" onSubmit={id === "tambah" ? addHandler : editHandler}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Jenis Kebiasaan </Form.Label>
                        <Form.Control
                            type="text"
                            placeholder=""
                            name="kebiasaan_name"
                            defaultValue={id === "tambah" ? "" : habit.kebiasaan_name}
                            onChange={id === "tambah" ? formHandler : formEditHandler}
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Bobot Penilaian </Form.Label>
                        <Form.Control
                            type="number"
                            placeholder=""
                            name="value"
                            defaultValue={id === "tambah" ? "" : habit.value}
                            onChange={id === "tambah" ? formHandler : formEditHandler}
                        />
                    </Form.Group>
                    <Button type="submit" className="m-1 btn-success float-end">
                        Simpan
                    </Button>
                </Form>
            </Card>
        </>
    );
};
export default HabitFormCMS;
