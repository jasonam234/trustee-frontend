import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import { getAllInsuranceType } from "../../../api/EndPointInsurance";
import {  getPackagePremiById, postPackagePremi, putPackagePremi } from "../../../api/EndPointPremi";
import CustomLoader from "../../loader/CustomLoader";
import SaveLoader from "../../loader/SaveLoader";
const PremiPackageForm = () => {
    const navigate = useNavigate();
    const [save, setSave] = useState(false);
    const [insuranceType, setInsuranceType] = useState([]);
    const [premiPackage, setPremiPackage] = useState({
        paket_id: 0,
        jenis: {
            jenis_id: 0,
        },
        paket_name: "",
        price: 0,
        period: 0,
        detail_paket: "",
    });

    useEffect(async () => {
        await getAllInsuranceType()
            .then((res) => {
                setInsuranceType(res.data.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [setInsuranceType]);

    const location = useLocation();
    const [, , id] = location.pathname.split("/");
    
const [pending, setPending] = useState(true);
    useEffect(async() => {
        await getPackagePremiById(id)
            .then((res) => {
                setPremiPackage(res.data.data);
            })
            .catch((err) => {
                console.log(err);
            }); 
            setPending(false);
    }, [setPremiPackage]);
    if (pending == true&& id!="tambah") {
        return (
            <div className="mt-7">
                <CustomLoader />
            </div>
        );
    }
    const formHandler = (evt) => {
        setPremiPackage((prevsValue) => {
            return { ...prevsValue, [evt.target.name]: evt.target.value };
        });
    };
    console.log(id);
    const formEditHandler = (evt) => {
        setPremiPackage((prevsValue) => {
            return {
                ...prevsValue,
                paket_id: Number(id),
                [evt.target.name]: evt.target.value,
            };
        });
    };
    const jenisHandler = (evt) => {
        if (evt.target.value == 0 || evt.target.value === "") {
            swal("", "Pilih jenis asuransi", "info");
            return;
        }
        setPremiPackage((prevsValue) => {
            return {
                ...prevsValue,
                jenis: {
                    ...prevsValue.jenis,
                    jenis_id: Number(evt.target.value),
                },
            };
        });
    };
    const addHandler = (evt) => {
        evt.preventDefault();
        if (premiPackage.paket_name === "") {
            swal("", "Nama Paket Asuransi tidak boleh kosong", "info");
        } else if (
            premiPackage.jenis.jenis_id === "" ||
            premiPackage.jenis.jenis_id === 0 ||
            premiPackage.jenis.jenis_id === null
        ) {
            swal("", "Pilihan Jenis Asuransi tidak boleh kosong", "info");
        } else if (premiPackage.period < 1) {
            swal("", "Periode tidak boleh kurang 1", "info");
        } else if (premiPackage.price < 10000) {
            swal("", "Harga  tidak boleh kurang dari Rp 10.000,00", "info");
        } else if (premiPackage.detail_paket < 1) {
            swal("", "Periode tidak boleh kurang 1", "info");
        } else {
            setSave(true)
            postPackagePremi(premiPackage)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data berhasil disimpan", "success");
                    navigate("/paket_premi");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    const editHandler = (evt) => {
        evt.preventDefault();
        if (premiPackage.paket_name === "") {
            swal("", "Nama Paket Asuransi tidak boleh kosong", "info");
        } else if (
            premiPackage.jenis.jenis_id === "" ||
            premiPackage.jenis.jenis_id === 0 ||
            premiPackage.jenis.jenis_id === null
        ) {
            swal("", "Pilihan Jenis Asuransi tidak boleh kosong", "info");
        } else if (premiPackage.period < 1) {
            swal("", "Periode tidak boleh kurang 1", "info");
        } else if (premiPackage.price < 10000) {
            swal("", "Harga  tidak boleh kurang dari Rp 10.000,00", "info");
        } else if (premiPackage.detail_paket < 1) {
            swal("", "Periode tidak boleh kurang 1", "info");
        } else {
            setSave(true)
            putPackagePremi(premiPackage)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data berhasil disimpan", "success");
                    navigate("/paket_premi");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    if (save == true) {
        return (
            <div className="mt-7">
                <SaveLoader />
            </div>
        );
    }
    return (
        <>
        <h5 className="text-secondary">Paket Premi</h5>
        <Card>
            <Form className="m-3" onSubmit={id === "tambah" ? addHandler : editHandler}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Nama</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder=""
                        name="paket_name"
                        defaultValue={id === "tambah" ? "" : premiPackage.paket_name}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Jenis</Form.Label>
                    <Form.Select name="select" type="select" onChange={jenisHandler} defaultValue={premiPackage.jenis.jenis_id}>
                        <option value="0">Pilih Jenis Asuransi</option>
                        {insuranceType.map((value) => {
                            return <option value={value.jenis_id}>{value.jenis_name}</option>;
                        })}
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Periode ( bulan ) </Form.Label>
                    <Form.Control
                        type="number"
                        placeholder=""
                        name="period"
                        defaultValue={id === "tambah" ? addHandler : premiPackage.period}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Harga ( Rp )</Form.Label>
                    <Form.Control
                        type="number"
                        placeholder=""
                        name="price"
                        defaultValue={id === "tambah" ? addHandler : premiPackage.price}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Detail</Form.Label>
                    <Form.Control
                        as="textarea"
                        placeholder=""
                        name="detail_paket"
                        defaultValue={id === "tambah" ? addHandler : premiPackage.detail_paket}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Button type="submit" className="m-1 btn-success float-end">
                    Simpan
                </Button>
            </Form>
        </Card></>
        
    );
};
export default PremiPackageForm;
