import { useState } from "react";
import { Card,Form,Button} from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import { putCarouselImg } from "../../../api/EndPointCMS";
import SaveLoader from "../../loader/SaveLoader";
const CarouselForm = () => {
    const navigate = useNavigate()
    const [carousel, setCarousel] = useState({
        image:""
    });
    
    const location= useLocation()
    const [, ,, name] = location.pathname.split("/");
    const [image, setImage] = useState();
    const [save, setSave] = useState(false);
    const upload =  async (evt) => {
        evt.preventDefault();
        if (image==""||image==0){
           await  swal("", "file tidak boleh kosong", "warning");
        }else{
            await setSave(true)
            const img = new FormData();
            img.append("image", image);
            await putCarouselImg(img)
                .then(async(respone) => { 
                    setSave(false)
                    await swal("", "Gambar berhasil disimpan", "success");
                    navigate("/konten")
                    console.log(respone);
                })
                .catch((err) => {
                    console.log(err);
                    swal("", "Gambar gagal disimpan", "error");
                    
                });
        }
  
    }
    if (save == true) {
        return (
            <div className="mt-7">
                <SaveLoader />
            </div>
        );
    }
   const imageHandler = async (evt) => {
        const filename = evt.target.value.replace(/^.*\\/, "");
        if (filename===name){
            setCarousel((prevsValue) => {
                return {
                    ...prevsValue,
                    image : filename
                }
                })
                setImage(evt.target.files[0])
        } else if (filename!=name) {
            await swal("", "ubah nama file menjadi "+name, "info");
            navigate("/konten")
        }
            
      }
    return (
        <Card>
            <Form className="m-3" onSubmit={upload}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Gambar</Form.Label>
                    <Form.Control type="file" name="name" multiple accept=".jpg" onChange={imageHandler} />
                </Form.Group>
                <div className="float-end">
                <Button type="submit" className="m-1 btn-success">Simpan</Button>
                {/* <Button className="m-1 btn-danger" onClick={rejectHandler}>Ditolak</Button> */}
                </div>
            </Form>
           
        </Card>
    );
};
export default CarouselForm