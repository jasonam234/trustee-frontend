import { Image, Form, Button, Col} from "react-bootstrap";
import swal from "sweetalert";
import { putPremiStatus } from "../../../api/EndPointInsurance";
import { source_image } from "../../../api/EndPointCMS";
import { useState } from "react";
import { postNotificationClaim } from "../../../api/EndPointClaim";
import { useNavigate } from "react-router";
import SaveLoader from "../../loader/SaveLoader";
import SendLoader from "../../loader/SendLoader";
const ClaimTransactionForm = ({ claimImage, premi, status }) => {
    const [reject, setReject] = useState(false);
    const [reason, setReason] = useState(0);
    const [save, setSave] = useState(false);
    const [send, setSend] = useState(false);
    const navigate = useNavigate();
    const rejectToReasonHandler = () => {
        setReject(true);
    };
    const reasonHandler = (evt) => {
        setReason(evt.target.value);
    };
    const rejectHandler = () => {
        if (reason === 0) {
            swal("", "Alasan wajib dipilih", "warning");
        } else {
            if (status === 5) {
                let obj = {
                    premi_id: premi,
                    status: {
                        status_id: 3,
                    },
                };
                setSave(true)
                putPremiStatus(obj)
                    .then(async (res) => {
                        await setSave(false)
                        await swal("", "Claim Berhasil dibatalkan", "success");
                        setSend(true)
                        postNotificationClaim(0, premi, reason)
                            .then(async (res) => {
                                await setSend(false)
                                await swal("", "Email Notifikasi Berhasil dikirim", "success");
                                navigate("/klaim");
                            })
                            .catch((err) => {
                                swal("", "Email Notifikasi gagal dikirim", "error");
                            });
                    })
                    .catch((err) => {
                        swal("", "Ubah status claim gagal", "error");
                    });
            } else {
                swal("", "Belum ada pengajuan atau status claim sudah diterima", "info");
            }
        }
    };
    const acceptHandler = () => {
        if (status === 5) {
            let obj = {
                premi_id: premi,
                status: {
                    status_id: 6,
                },
            };
            setSave(true)
            putPremiStatus(obj)
                .then(async (res) => {
                    await setSave(false) 
                    await swal("", "Klaim Berhasil diterima", "success");
                    setSend(true)

                    postNotificationClaim(1, premi, 0)
                        .then(async (res) => {
                            await setSend(false)
                            await swal("", "Email Notifikasi Berhasil dikirim", "success");
                            navigate("/klaim");
                        })
                        .catch((err) => {
                            swal("", "Email Notifikasi gagal dikirim", "error");
                        });
                        // setSave(true)
                })
                .catch((err) => {
                    swal("", "Ubah status klaim gagal", "error");
                });
        } else {
            swal("", "Belum ada pengajuan atau status claim sudah diterima", "info");
        }
    };

    if (save===true) {
        return (
            <div className="">
                <SaveLoader/>
            </div>
        );
    }
    if (send===true) {
        return (
            <div className="">
                <SendLoader />
            </div>
        );
    }

    return (
        <Form className="m-3">
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Bukti Claim</Form.Label>
            </Form.Group>

            <Col>
                <a href={source_image + claimImage}>
                    <Image src={source_image + claimImage} thumbnail />
                </a>
            </Col>
            {reject === true ? (
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Alasan Penolakan</Form.Label>
                    <Form.Select name="select" type="select" onChange={reasonHandler}>
                        <option value="0">Pilih Alasan Penolakan</option>
                        <option value="1">Kualitas gambar kurang baik</option>
                        <option value="2">Tanggal pada dokumen tidak valid</option>
                        <option value="3">Dokumen tidak valid</option>
                    </Form.Select>
                    <Button className="mt-2 btn-danger" onClick={rejectHandler}>
                        Lanjutkan Penolakan
                    </Button>
                </Form.Group>
            ) : (
                <></>
            )}

            {reject === false ? (
                <div className="float-end mt-5">
                    <Button className="m-1 btn-success" onClick={acceptHandler}>
                        Klaim diterima
                    </Button>
                    <Button className="m-1 btn-danger" onClick={rejectToReasonHandler}>
                        Klaim ditolak
                    </Button>
                </div>
            ) : (
                <></>
            )}
        </Form>
    );
};
export default ClaimTransactionForm;
