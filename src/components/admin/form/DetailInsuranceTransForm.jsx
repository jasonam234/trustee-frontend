import { Form } from "react-bootstrap";
const DetailInsuranceTransForm = ({paket,jenis,user,dates}) => {
    const formatter = new Intl.NumberFormat('id-Id', {
        style: 'currency',
        currency: 'IDR',})
        
    return (
        <Form className="m-3">
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Tanggal Pengajuan</Form.Label>
                <Form.Control type="text" placeholder="" value={dates} disabled/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Jenis</Form.Label>
                <Form.Select disabled>
                    <option>{jenis.jenis_name}</option>
                </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Paket</Form.Label>
                <Form.Select disabled>
                    <option>{paket.paket_name}</option>
                </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Jangka</Form.Label>
                <Form.Select disabled>
                    <option>{paket.period} Bulan</option>
                </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Harga</Form.Label>
                <Form.Control type="text" placeholder="" value={formatter.format(paket.price)} disabled/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Nama Pengguna</Form.Label>
                <Form.Control type="text" placeholder="" value={user.full_name} disabled/>
            </Form.Group>
        </Form>
    );
};
export default DetailInsuranceTransForm;
