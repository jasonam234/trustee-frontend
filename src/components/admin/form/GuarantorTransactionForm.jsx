import { Col, Form, Image } from "react-bootstrap";
import { KTP_IMG } from "../../../base";
const GuarantorTransactionForm = ({ data }) => {
    return (
        <Form className="m-3">
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Nama</Form.Label>
                <Form.Control type="text" placeholder="" value={data.full_name} disabled />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Tanggal Lahir</Form.Label>
                <Form.Control type="text" placeholder="" value={data.birth_date} disabled />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>No KTP</Form.Label>
                <Form.Control type="text" placeholder="" value={data.ktp} disabled />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Foto KTP</Form.Label>
                <Col>
                <a href={KTP_IMG+data.ktp_image}>
                    <Image src={KTP_IMG + data.ktp_image} thumbnail />
                    </a>
                </Col>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Telepon</Form.Label>
                <Form.Control type="text" placeholder="" value={data.phone} disabled />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Alamat</Form.Label>
                <Form.Control as="textarea" rows={3} value={data.address} disabled />
            </Form.Group>
        </Form>
    );
};
export default GuarantorTransactionForm;
