import { Card,Form,Button } from "react-bootstrap";
const InsuranceTypeForm = () => {
    return (
        <Card>
            <Form className="m-3">
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Jenis</Form.Label>
                    <Form.Control type="text" placeholder="" />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </Card>
    );
};
export default InsuranceTypeForm;