import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import { getAllDiseases, getDiseasesById, postDiseases, putDiseases } from "../../../api/EndPointCMS";
import CustomLoader from "../../loader/CustomLoader";
import SaveLoader from "../../loader/SaveLoader";
const DiseasesHistoryFomCMS = () => {
    const navigate = useNavigate();
    const [diseases, setDiseases] = useState({
        penyakit_id: 0,
        penyakit_name: "",
        value: 0,
    });
    const location = useLocation();
    const [, , id] = location.pathname.split("/");
    const [pending, setPending] = useState(true);
    const [save, setSave] = useState(false);
    useEffect(async() => {
        await getDiseasesById(id)
            .then((res) => {
                setDiseases(res.data.data);
            })
            .catch((err) => {
                console.log(err);
            });
            setPending(false);
    }, [setDiseases]);
    if (pending == true&& id!="tambah") {
        return (
            <div className="mt-7">
                <CustomLoader />
            </div>
        );
    }
    if (save == true) {
        return (
            <div className="mt-7">
                <SaveLoader />
            </div>
        );
    }
    const formHandler = (evt) => {
        setDiseases((prevsValue) => {
            return { ...prevsValue, [evt.target.name]: evt.target.value };
        });
    };
    const formEditHandler = (evt) => {
        setDiseases((prevsValue) => {
            return {
                ...prevsValue,
                penyakit_id: id,
                [evt.target.name]: evt.target.value,
            };
        });
    };

    const addHandler = (evt) => {
        evt.preventDefault();
        if (diseases.penyakit_name === "") {
            swal("", "Jenis Penyakit tidak boleh kosong", "warning");
        } else if (diseases.value === "" || diseases.value === 0) {
            swal("", "Bobot tidak boleh kosong atau bernilai 0", "warning");
        } else if (diseases.penyakit_name === "" || diseases.value === "") {
            swal("", "Data tidak boleh kosong", "warning");
        } else if (diseases.value <= 0) {
            swal("", "Bobot nilai tidak boleh kurang dari 1", "warning");
        } else if (diseases.value > 30) {
            swal("", "Bobot nilai tidak boleh lebih dari dari 30", "warning");
        } else {
            setSave(true)
            postDiseases(diseases)
                .then(async (res) => {
                    await setSave(false)
                    console.log(res);
                    await swal("", "Data Berhasil disimpan", "success");
                    navigate("/penyakit");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    const editHandler = (evt) => {
        evt.preventDefault();
        if (diseases.penyakit_name === "") {
            swal("", "Jenis Penyakit tidak boleh kosong", "warning");
        } else if (diseases.value === "" || diseases.value === 0) {
            swal("", "Bobot tidak boleh kosong atau bernilai 0", "warning");
        } else if (diseases.penyakit_name === "" || diseases.value === "") {
            swal("", "Data tidak boleh kosong", "warning");
        } else if (diseases.value <= 0) {
            swal("", "Bobot nilai tidak boleh kurang dari 1", "warning");
        } else if (diseases.value > 30) {
            swal("", "Bobot nilai tidak boleh lebih dari dari 30", "warning");
        } else {
            setSave(true)
            putDiseases(diseases)
                .then(async (res) => {            
                    await setSave(true)
                    await swal("", "Data Berhasil disimpan", "success");
                    navigate("/penyakit");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    return (
       <>
       <h5 className="text-secondary">Jenis Penyakit</h5>
        <Card>
            <Form className="m-3" onSubmit={id === "tambah" ? addHandler : editHandler}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Jenis Penyakit </Form.Label>
                    <Form.Control
                        type="text"
                        placeholder=""
                        name="penyakit_name"
                        defaultValue={id === "tambah" ? "" : diseases.penyakit_name}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Bobot Penilaian </Form.Label>
                    <Form.Control
                        type="number"
                        placeholder=""
                        name="value"
                        defaultValue={id === "tambah" ? "" : diseases.value}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Button type="submit" className="m-1 btn-success float-end">
                    Simpan
                </Button>
            </Form>
        </Card>
       </>
    );
};
export default DiseasesHistoryFomCMS;
