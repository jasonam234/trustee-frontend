import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import { postRelation, putRelation } from "../../../api/EndPointInsurance";
import {  getRelationById } from "../../../api/EndPointCMS";
import CustomLoader from "../../loader/CustomLoader";
import SaveLoader from "../../loader/SaveLoader";
const RelationshipForm = () => {
    const navigate = useNavigate();
    const [save, setSave] = useState(false);
    const [relation, setRelation] = useState({
        relation_id: 0,
        relation_name: "",
    });
    const location = useLocation();
    const [, , id] = location.pathname.split("/");
    const [pending, setPending] = useState(true);
    
    useEffect(async() => {
        await getRelationById(id)
            .then((res) => {
                setRelation(res.data.data);
            })
            .catch((err) => {
                console.log(err);
            });
            setPending(false);
    }, [setRelation]);
    if (pending == true&& id!="tambah") {
        return (
            <div className="mt-7">
                <CustomLoader />
            </div>
        );
    }
    if (save == true) {
        return (
            <div className="mt-7">
                <SaveLoader />
            </div>
        );
    }
    const formHandler = (evt) => {
        setRelation((prevsValue) => {
            return {
                ...prevsValue,
                relation_name: evt.target.value,
            };
        });
    };
    const formEditHandler = (evt) => {
        setRelation((prevsValue) => {
            return {
                ...prevsValue,
                relation_id: id,
                relation_name: evt.target.value,
            };
        });
    };

    const addHandler = (evt) => {
        evt.preventDefault();
        if (relation.relation_name === "") {
            swal("", "Lengkapi data", "warning");
        } else {
            setSave(true)
            postRelation(relation)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data Berhasil disimpan", "success");
                    navigate("/hubungan");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    const editHandler = (evt) => {
        evt.preventDefault();
        if (relation.relation_name === "") {
            swal("", "Kolom hubungan tidak boleh kosong", "warning");
        } else {
            setSave(true)
            putRelation(relation)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data Berhasil disimpan", "success");
                    navigate("/hubungan");
                })
                .catch((err) => {
                    swal("", "Data Gagal disimpan", "error");
                });
        }
    };
    return (
        <> 
        <h5 className="text-secondary">Hubungan</h5>
        <Card>
            <Form className="m-3" onSubmit={id === "tambah" ? addHandler : editHandler}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Hubungan</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder=""
                        defaultValue={id === "tambah" ? (""): relation.relation_name}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Button type="submit" className="m-1 btn-success float-end">
                    Simpan
                </Button>
            </Form>
        </Card></>
       
    );
};
export default RelationshipForm;
