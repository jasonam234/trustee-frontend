import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import { getAllReview, putReview } from "../../../api/EndPointCMS";
import CustomLoader from "../../loader/CustomLoader";
import SaveLoader from "../../loader/SaveLoader";
const ReviewForm = () => {
    const location = useLocation();
    const [, , , id] = location.pathname.split("/");
    const [save, setSave] = useState(false);
    const navigate =useNavigate()
    const [review, setReview] = useState({
        cms_id: id,
        reviewer_name: "",
        review_content: "",
    });
    const reviewerHandler = (evt) => {
        setReview((prevsValue) => {
            return {
                ...prevsValue,
                reviewer_name: evt.target.value,
            };
        });
    };
    const [pending, setPending] = useState(true);
    
    useEffect(async() => {
        await getAllReview()
            .then((res) => {
                setReview(res.data.data[id-1]);
            })
            .catch((err) => {
                console.log(err);
            });
            setPending(false);
    }, [setReview]);
    if (pending == true) {
        return (
            <div className="mt-7">
                <CustomLoader />
            </div>
        );
    }
    if (save == true) {
        return (
            <div className="mt-7">
                <SaveLoader />
            </div>
        );
    }
    const descriptionHandler = (evt) => {
        setReview((prevsValue) => {
            return {
                ...prevsValue,
                review_content: evt.target.value,
            };
        });
    };
    const saveHandler = (evt) => {
        evt.preventDefault();
        if(review.reviewer_name === "" ){
            swal("", "Nama boleh kosong", "warning");
        } else if(review.review_content === ""){
            swal("", "Cont tidak boleh kosong", "warning");
        } else if(review.reviewer_name === "" && review.review_content === "") {
            swal("", "Data tidak boleh kosong", "warning");
        } else {
            setSave(true)
            putReview(review)
                .then(async(res) => {
                    await setSave(false)
                    await swal("", "Review berhasil disimpan", "success");
                    navigate("/konten")
                })
                .catch((err) => {
                    swal("", "Review gagal disimpan", "error");
                    alert(err);
                });
        }   
    };
    return (
        <>
                <h5 className="mt-5 text-secondary">Review</h5>
        <Card>
            <Form className="m-3" onSubmit={saveHandler}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Nama</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder=""
                        defaultValue={review.reviewer_name}
                        onChange={reviewerHandler}
                        required
                    ></Form.Control>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Deskripsi</Form.Label>
                    <Form.Control
                        as="textarea"
                        placeholder=""
                        defaultValue={review.review_content}
                        onChange={descriptionHandler}
                        required
                    ></Form.Control>
                </Form.Group>
                <Button type="submit" className="btn-success float-end">
                    Simpan
                </Button>
            </Form>
        </Card>
        </>

    );
};
export default ReviewForm;
