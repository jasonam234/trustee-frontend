import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import { getInsuranceTypeById, postInsuranceType, putInsuranceType } from "../../../api/EndPointInsurance";
import CustomLoader from "../../loader/CustomLoader";
import SaveLoader from "../../loader/SaveLoader";
const InsuranceTypeForm = () => {
    const navigate = useNavigate();
    const [save, setSave] = useState(false);
    const [jenis, setjenis] = useState({
        jenis_id: 0,
        jenis_name: "",
        detail_jenis: "",
    });
    const location = useLocation();
    const [, , id] = location.pathname.split("/");
    const [pending, setPending] = useState(true);
    useEffect(async() => {
         await getInsuranceTypeById(id)
            .then((res) => {
                setjenis(res.data.data);
            })
            .catch((err) => {
                console.log(err);
            });
            setPending(false);
    }, [setjenis]);
    if (pending == true && id!="tambah" ) {
        return (
            <div className="mt-7">
                <CustomLoader />
            </div>
        );
    }
    if (save===true) {
        return (
            <div className="mt-7">
                <SaveLoader/>
            </div>
        );
    }
    const formHandler = (evt) => {
        setjenis((prevsValue) => {
            return { ...prevsValue, [evt.target.name]: evt.target.value };
        });
    };
    const formEditHandler = (evt) => {
        setjenis((prevsValue) => {
            return {
                ...prevsValue,
                jenis_id: id,
                [evt.target.name]: evt.target.value,
            };
        });
    };

    const addHandler = (evt) => {
        evt.preventDefault();
        if (jenis.jenis_name === "") {
            swal("", "Jenis Asuransi tidak boleh kosong", "warning");
        } else if (jenis.detail_jenis === "") {
            swal("", "Detail Asuransi tidak boleh kosong", "warning");
        } else if (jenis.jenis_name === "" && jenis.detail_jenis === "") {
            swal("", "Data tidak boleh kosong", "warning");
        } else {
            setSave(true)
            postInsuranceType(jenis)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data berhasil disimpan", "success");
                    navigate("/paket_premi/tambah");
                })
                .catch((err) => {
                    swal("", "Data gagal disimpan", "error");
                });
        }
    };
    const editHandler = (evt) => {
        evt.preventDefault();
        if (jenis.jenis_name === "") {
            swal("", "Jenis Asuransi tidak boleh kosong", "warning");
        } else if (jenis.detail_jenis === "") {
            swal("", "Detail Asuransi tidak boleh kosong", "warning");
        } else if (jenis.jenis_name === "" && jenis.detail_jenis === "") {
            swal("", "Data tidak boleh kosong", "warning");
        } else {
            setSave(true)
            putInsuranceType(jenis)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Data berhasil disimpan", "success");
                    navigate("/jenis_asuransi");
                })
                .catch((err) => {
                    swal("", "Data gagal disimpan", "error");
                });
        }
    };
    return (
        <>
        <h5 className="text-secondary">Jenis Asuransi</h5>
        <Card>
            <Form className="m-3" onSubmit={id === "tambah" ? addHandler : editHandler}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Jenis</Form.Label>
                    <Form.Control
                        type="text"
                        name="jenis_name"
                        placeholder=""
                        defaultValue={id === "tambah" ? "" : jenis.jenis_name}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Detail</Form.Label>
                    <Form.Control
                        as="textarea"
                        placeholder=""
                        name="detail_jenis"
                        defaultValue={id === "tambah" ? "" : jenis.detail_jenis}
                        onChange={id === "tambah" ? formHandler : formEditHandler}
                    />
                </Form.Group>
                <Button type="submit" className="m-1 btn-success float-end">
                    Simpan
                </Button>
            </Form>
        </Card></>
        
    );
};
export default InsuranceTypeForm;
