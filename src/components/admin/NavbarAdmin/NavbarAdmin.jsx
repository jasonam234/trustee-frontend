import { Navbar, Nav, Container, NavDropdown } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import "./NavbarAdmin.css";
import logo from "../../../logo.png";
import { Button } from "react-bootstrap";
import swal from "sweetalert";

const NavbarAdmin = () => {
    const navigate = useNavigate();
    const requestHandler = () => {
        navigate("/premi");
    };
    const claimHandler = () => {
        navigate("/klaim");
    };
    const reportHandler = () => {
        navigate("/laporan");
    };
    const contentHandler = () => {
        navigate("/konten");
    };
    const typeHandler = () => {
        navigate("/jenis_asuransi");
    };
    const packageHandler = () => {
        navigate("/paket_premi");
    };
    const relationsHandler = () => {
        navigate("/hubungan");
    };
    const diseaseshandler = () => {
        navigate("/penyakit");
    };
    const habitHandler = () => {
        navigate("/kebiasaan");
    };
    const logoutHandler = () => {
        swal("", "Apakah yakin keluar sekarang",  "info", {
            buttons: {
              cancel: "Batalkan",
              catch: {
                text: "Yakin",
                value: "catch",
              },
              
            },
          })
          .then((value) => {
            switch (value) {
                         
              case "catch":
                localStorage.clear();
                window.location.reload();
               
                break;
           
              default:;
            }
          }, );
    };
    return (
        <div>
            <>
                <Navbar expand="lg" className="navbar-bg navbar-item"  fixed="top">
                    <Container fluid>
                        <Navbar.Brand as={Link} to="/">
                            <img width="100px" src={logo} alt="logo" />
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="navbarScroll" />
                        <Navbar.Collapse id="navbarScroll">
                            <Nav className="me-auto my-2 my-lg-0" navbarScroll>
                                <Nav.Link className="nav-links" onClick={requestHandler} to="/">
                                    Premi
                                </Nav.Link>
                                <Nav.Link className="nav-links" onClick={claimHandler} to="/">
                                    Klaim
                                </Nav.Link>
                                <Nav.Link className="nav-links" onClick={reportHandler} to="/">
                                    Laporan
                                </Nav.Link>
                                <Nav.Link className="nav-links" onClick={contentHandler} to="/">
                                    Konten
                                </Nav.Link>

                                <NavDropdown title="Produk" id="nav-dropdown" className="nav-links">
                                    <NavDropdown.Item eventKey="4.1" onClick={typeHandler}>
                                        Jenis Asuransi
                                    </NavDropdown.Item>
                                    <NavDropdown.Item eventKey="4.2" onClick={packageHandler}>
                                        Paket Asuransi
                                    </NavDropdown.Item>
                                </NavDropdown>
                                <NavDropdown title="Formulir" id="nav-dropdown" className="nav-links">
                                    <NavDropdown.Item eventKey="4.3" onClick={relationsHandler}>
                                        Hubungan
                                    </NavDropdown.Item>
                                    <NavDropdown.Item eventKey="4.4" onClick={diseaseshandler}>
                                        Penyakit tertanggung
                                    </NavDropdown.Item>
                                    <NavDropdown.Item eventKey="4.5" onClick={habitHandler}>
                                        Kebiasaan tertanggung
                                    </NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                            <Nav className="justify-content-end">
                                <Nav.Link>
                                    <Button
                                        size="sm"
                                        className="btn-danger nav-links"
                                        onClick={logoutHandler}
                                    >
                                        Keluar
                                    </Button>
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </>
        </div>
    );
};

export default NavbarAdmin;
