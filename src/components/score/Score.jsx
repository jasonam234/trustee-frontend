import { Card } from "react-bootstrap";
const Score = ({score}) => {
    const color = 
    score>=80 ? ("bg-success box-custom m-auto")
        :score>=70 && score<=79 ? ("bg-info box-custom m-auto")
        :score>=60 && score<=69 ? ("bg-warning box-custom m-auto")
        :score>=50 && score<=59 ? ("bg-danger box-custom m-auto")
        :("bg-dark box-custom m-auto")
    return (
        <div className={color}>
            <h5>Score : {score}</h5>
        </div>
       
    );
};
export default Score;
