import api from ".";
import jwt_decode from "jwt-decode";

let token = localStorage.getItem("token")

export async function loginNew(form) {
  let fetching = await api.post(`/authenticate`, form);
  return fetching;
}

export function login(form) {
  api.post(`/authenticate`, form).then((res) => {
    let token = res.data.token;
    localStorage.setItem("token", token);
    let decode = jwt_decode(token);
    localStorage.setItem("user_id", decode.user_id);
    localStorage.setItem("role", decode.role);
    localStorage.setItem("exp", decode.exp);
    return { token, decode };
  });
}
export const registerNew = async (form) => {
  let fetching = await api.post(`/register`, form);
  return fetching;
};

export function register(form) {
  api
    .post(`/register`, form)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}

export const getData = async (url) => {
  let fetching = await api.get(url, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
};

export const getDataNoToken =async (url) => {
  let fetching = await api.get(url);
  return fetching
}

export const postData = async (url, data) => {
  // let token = await JWT_TOKEN;
  console.log(token);
  let fetching = await api.post(url, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
};

export const postImage = (url, data) => {
  // let token = JWT_TOKEN;
  let fetching = api.post(url, data, {
    headers: {
      "content-type": "multipart/form-data",
      Authorization: `Bearer ${token}`,
    },
  });
  return fetching;
};

export const putData = async (url, data) => {
  // let token = await JWT_TOKEN;
  let fetching = await api.put(url, data, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return fetching;
};