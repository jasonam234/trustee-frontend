import api from ".";
let token = localStorage.getItem("token")

export async function getAllPremi() {
    let fetching = await api.get(`/premi/get/premi`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getPremiById(id) {
    let fetching = await api.get(`/premi/get/premi/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getAllPackagePremi() {
    let fetching = await api.get(`/premi/get/paket`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getPackagePremiById(id) {
  let fetching = await api.get(`premi/get/paket/paket_id/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function putPackagePremi(data) {
    let fetching = await api.put(`/premi/put/paket`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function postPackagePremi(data) {
    let fetching = await api.post(`/premi/post/paket`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function deletePackagePremi(id) {
    let fetching = await api.delete(`/premi/delete/paket/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getKTPImage(ktp_image) {
  let fetching = await api.delete(`/user/get/ktp/image/${ktp_image}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}

