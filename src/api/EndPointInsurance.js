import api from ".";
let token = localStorage.getItem("token")

export async function getAllInsuranceType() {
    let fetching = await api.get(`/premi/get/jenis`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getInsuranceTypeById(id) {
  let fetching = await api.get(`/premi/get/jenis/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function putPremiStatus(data) {
    let fetching = await api.put(`/premi/put/premi/status`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function postRelation(data) {
    let fetching = await api.post(`/user/post/relation`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function putRelation(data) {
    let fetching = await api.put(`/user/put/relation`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function deleteRelation(id) {
    let fetching = await api.delete(`/user/delete/relation/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function postInsuranceType(data) {
    let fetching = await api.post(`/premi/post/jenis`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function putInsuranceType(data) {
    let fetching = await api.put(`/premi/put/jenis`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function deleteInsuranceType(id) {
    let fetching = await api.delete(`/premi/delete/jenis/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}