import api from ".";
let token = localStorage.getItem("token")

export async function getAllReview() {
    let fetching = await api.get(`/admin/get/cms/review`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}

export async function getCarouselOne() {
    let fetching = await api.get(`/admin/cms/image/carousel_one.jpg`)
    return fetching;
}

export async function getCarouselTwo() {
    let fetching = await api.get(`/admin/cms/image/carousel_two.jpg`)
    return fetching;
}

export async function putReview(data) {
    let fetching = await api.put(`/admin/put/cms/review`,data, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function putCarouselImg(data) {
    let fetching = await api.put(`/admin/update/cms/image`,data, {
        headers: {
          "content-type": "multipart/form-data",  Authorization: `Bearer ${token}`
        },
      });
    return fetching;
}

export async function getAllRelation() {
    let fetching = await api.get(`/user/get/relation`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getRelationById(id) {
  let fetching = await api.get(`/user/get/relation/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export const source_image = "http://localhost:8081/api/admin/cms/image/"

export async function getAllDiseases() {
  let fetching = await api.get(`/admin/get/penyakit`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function getDiseasesById(id) {
  let fetching = await api.get(`/admin/get/penyakit/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function postDiseases(data) {
  let fetching = await api.post(`/admin/post/penyakit`,data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function putDiseases(data) {
  let fetching = await api.put(`/admin/put/penyakit`,data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function deleteDiseases(id) {
  let fetching = await api.delete(`/admin/delete/penyakit/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}

export async function getAllHabit() {
  let fetching = await api.get(`/admin/get/kebiasaan`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function getHabitById(id) {
  let fetching = await api.get(`/admin/get/kebiasaan/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function postHabit(data) {
  let fetching = await api.post(`/admin/post/kebiasaan`,data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function putHabit(data) {
  let fetching = await api.put(`/admin/put/kebiasaan`,data, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}
export async function deleteHabit(id) {
  let fetching = await api.delete(`/admin/delete/kebiasaan/${id}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
  return fetching;
}