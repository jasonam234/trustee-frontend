import api from ".";
let token = localStorage.getItem("token")
export function getCountAllInsurance() {
    let fetching =  api.get(`/premi/get/premi/value`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching
    
}
export async function getCountInsuranceType(id) {
    let fetching = await api.get(`/premi/get/premi/value/jenis/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getListReport() {
    let fetching = await api.get(`/premi/get/premi/total_value`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
