import api from ".";
let token = localStorage.getItem("token")
export async function getAllPremi() {
    let fetching = await api.get(`/admin/get/premi`,{
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function getAllClaim() {
    let fetching = await api.get(`/premi/get/premi/status/5`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}

export async function getAllPackagePremi() {
    let fetching = await api.get(`/admin/pakets`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    return fetching;
}
export async function postNotificationClaim(status,premi_id,reason) {
  let fetching = await api.post(`/premi/post/premi/klaim/email/${status}/${premi_id}/${reason}`, {
      headers: { Authorization: `Bearer ${token}`},
    });
  return fetching;
}
