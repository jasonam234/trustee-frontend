import { Col, Container } from "react-bootstrap";
import ReviewForm from "../../components/admin/form/ReviewForm";
const ReviewPage = () => {
    return (
        <Container className="mt-7">
            <Col lg={8} className="m-auto">
           <ReviewForm />
            </Col>
            
        </Container>
    );
};

export default ReviewPage;
