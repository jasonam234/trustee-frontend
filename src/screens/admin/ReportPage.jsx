import { Container, Card, Row } from "react-bootstrap";
import React from "react";
import CardTotal from "../../components/admin/card/CardTotal";
import CardInsuranceType from "../../components/admin/card/CardInsuranceType";
import ReportList from "../../components/admin/list/ReportList";
const ReportPage = () => {
    return (
        <div>
            <Container className="mt-7">
            <h5 className="text-secondary">Total Seluruh Premi</h5>
                <Row>
                    <CardTotal />
                </Row>
                <Row>
                    <CardInsuranceType />
                </Row>
                <h5 className="mt-5 text-secondary">Laporan Premi perhari</h5>
                <Card className="table-responsive p-3">
                    <ReportList />
                </Card>
            </Container>
        </div>
    );
};

export default ReportPage;
