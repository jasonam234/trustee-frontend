import { Row, Card, Container } from "reactstrap";
import CardTotal from "../../components/admin/card/CardTotal";
import ClaimList from "../../components/admin/list/ClaimList";
import CardInsuranceType from "../../components/admin/card/CardInsuranceType";
import PremiList from "../../components/admin/list/PremiList";
const AdminDashboardPage = () => {
    var refresh = window.localStorage.getItem("refresh");
    if (refresh === null) {
        window.location.reload();
        window.localStorage.setItem("refresh", "1");
    }
    return (
        <Container className="mt-7">
            <h3  className="text-secondary">Beranda</h3>
            <hr />
            <Row>
                <h5 className="mt-3 text-secondary">Total Seluruh Premi</h5>
                <CardTotal />
            </Row>
            <Row>
                <CardInsuranceType />
            </Row>
            <hr />
            <h5 className="mt-5 text-secondary">Daftar Premi</h5>
            <Card className="table-responsive p-3">
                <PremiList />
            </Card>
            <hr />
            <h5 className="mt-5 text-secondary">Daftar Klaim Premi</h5>
            <Card className="table-responsive p-3">
                <ClaimList />
            </Card>
        </Container>
    );
};
export default AdminDashboardPage;
