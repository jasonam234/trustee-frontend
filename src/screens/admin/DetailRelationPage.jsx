import { Col, Container } from "react-bootstrap";
import RelationshipForm from "../../components/admin/form/RelationshipForm";
const DetailRelationPage = (data) => {
    console.log(data);
    return (
        <Container className="mt-7">
            <Col lg={8} className="m-auto mt-5 muted">
                <RelationshipForm />
            </Col>
        </Container>
    );
};
export default DetailRelationPage;
