import { Col, Container } from "react-bootstrap";
import InsuranceTypeForm from "../../components/admin/form/InsuranceTypeForm";
const DetailInsuranceType = () => {
    return (
        <Container className="mt-7">
            <Col lg={8} className="m-auto mt-5 muted">
                <InsuranceTypeForm />
            </Col>
        </Container>
    );
};
export default DetailInsuranceType;
