import { Col, Container } from "react-bootstrap";
import ClaimTransactionForm from "../../components/admin/form/ClaimTransactionForm";
const ClaimDetailPage = () => {
    return (
        <Container className="mt-7">
            <Col lg={8} className="m-auto mt-5">
                <h5 className="text-secondary">Detail Klaim</h5>
                <ClaimTransactionForm />
            </Col>
        </Container>
    );
};
export default ClaimDetailPage;
