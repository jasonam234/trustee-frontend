import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { IoAddCircle } from "react-icons/io5";
import { Link } from "react-router-dom";
import InsurancePackageList from "../../components/admin/list/InsurancePackageList";
const PremiPage = () => {
    const style = { color: "white" };
    return (
        <div>
            <Container className="mt-7">
                <Row>
                    <Col>
                        <h5 className="mt-5 text-secondary">Paket Premi Asuransi</h5>
                    </Col>
                    <Col>
                        <Link to="/paket_premi/tambah">
                            <Button variant="primary mt-5 float-end" size="sm">
                                Tambah <IoAddCircle style={style} size={20} />
                            </Button>
                        </Link>
                    </Col>
                </Row>
                <Card className="table-responsive p-3 mt-3">
                    <InsurancePackageList/>
                </Card>
            </Container>
        </div>
    );
};

export default PremiPage;
