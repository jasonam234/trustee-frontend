import { Card, Container, Tab, Tabs } from "react-bootstrap";
import DataTable from "react-data-table-component";
import CarouselColumns from "../../components/admin/columns/CarouselColumns";
import React from "react";
import CarouselList from "../../components/admin/list/CarouselList";
import ReviewList from "../../components/admin/list/ReviewList";
import DataCarouselList from "../../components/admin/list/DataCarouselList";

const ContentPage = () => {
    return (
        <Container className="mt-7">
            <Tabs
                defaultActiveKey="carousel"
                id="uncontrolled-tab-example"
                className="mt-5 mb-3"
                text="black"
            >
                <Tab eventKey="carousel" title="Carousel">
                    <Card className="table-responsive p-3">
                       <DataCarouselList/>
                    </Card>
                </Tab>
                <Tab eventKey="review" title="Ulasan">
                    <Card className="table-responsive p-3">
                        <ReviewList/>
                    </Card>
                </Tab>
            </Tabs>
        </Container>
    );
};

export default ContentPage;
