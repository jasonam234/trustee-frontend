import { Container,Card } from "react-bootstrap";
import ClaimList from "../../components/admin/list/ClaimList";
const ClaimListPage = () => {
    return (
        <div>
          <Container className="mt-7">
          <h5 className="mt-5 text-secondary">Daftar Klaim</h5>
          <Card className="table-responsive p-3">
               <ClaimList/>
            </Card>
          </Container>
        </div>
    );
};
export default ClaimListPage;
