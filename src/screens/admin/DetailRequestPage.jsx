import { useEffect, useState } from "react";
import { Card, Col, Container, Row, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router";
import swal from "sweetalert";
import { putPremiStatus } from "../../api/EndPointInsurance";
import { getPremiById } from "../../api/EndPointPremi";
import ClaimTransactionForm from "../../components/admin/form/ClaimTransactionForm";
import DetailInsuranceTransForm from "../../components/admin/form/DetailInsuranceTransForm";
import GuarantorTransactionForm from "../../components/admin/form/GuarantorTransactionForm";
import HierTransactionForm from "../../components/admin/form/HierTransactionForm";
import CustomLoader from "../../components/loader/CustomLoader";
import SaveLoader from "../../components/loader/SaveLoader";
import Score from "../../components/score/Score";
import Status from "../../components/status/Status";
import { premiActions } from "../../store/premi";
const DetailRequestPage = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const navigate = useNavigate();
    const [save, setSave] = useState(false);
    const [pending, setPending] = useState(true);
    const [, , premiId] = location.pathname.split("/");
    const premiDetail = useSelector((state) => state.premi.premiDetail);

    useEffect(async() => {
            await getPremiById(premiId)
                .then((res) => {
                    dispatch(premiActions.premiDetail(res.data.data));
                })
                .catch((err) => {
                    console.log(err);
                });
            setPending(false);
    }, [dispatch]);
    if (save===true) {
        return (
            <div className="mt-7">
                <SaveLoader/>
            </div>
        );
    }
    if (pending == true) {
        return (
            <div className="mt-7">
                <CustomLoader />
            </div>
        );
    }
    const acceptHandler = () => {
        if (premiDetail.status.status_id === 1) {
            let obj = {
                premi_id: premiDetail.premi_id,
                status: {
                    status_id: 2,
                },
            };
            setSave(true)
            putPremiStatus(obj)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Premi disetujui", "success");
                    navigate("/premi");
                })
                .catch((err) => {
                    swal("", "Ubah status gagal", "error");
                });
        } else {
            swal("", "Premi sudah aktif atau telah di setujui", "info");
        }
    };
    const rejectHandler = () => {
        if (premiDetail.status.status_id === 1 || premiDetail.status.status_id === 2) {
            let obj = {
                premi_id: premiDetail.premi_id,
                status: {
                    status_id: 4,
                },
            };
            setSave(true)
            putPremiStatus(obj)
                .then(async (res) => {
                    await setSave(false)
                    await swal("", "Premi ditolak", "success");
                    navigate("/premi");
                })
                .catch((err) => {
                    swal("", "Ubah status gagal", "error");
                });
        } else if (premiDetail.status.status_id === 3) {
            swal("", "Premi sudah ditolak", "info");
        } else if (premiDetail.status.status_id === 2) {
            swal("", "Premimasih menunggu pembayaran", "info");
        } else {
            swal("", "Premi sudah aktif atau telah di setujui", "info");
        }
    };

    return (
        <Container className="mt-7">
            <Col lg={10} className="m-auto  ">
            <h5 className="mt-5 text-secondary">Detail Pengajuan Premi</h5>
            <Card className="p-5">
                <Row className="m-2">
                    <Col lg={6} className="m-auto pb-3">
                        <Status status={premiDetail.status} />
                    </Col>
                    <Col lg={6} className="m-auto  pb-3">
                        <Score score={premiDetail.nilai} />
                    </Col>
                </Row>
                <Col lg={12} className="m-auto">
                    <DetailInsuranceTransForm paket={premiDetail.paket} jenis={premiDetail.paket.jenis} user={premiDetail.user} dates={premiDetail.created_at}/>
                    <Row className="mt-1">
                        <Col lg={6}>
                            <p className="fw-bold text-center">Tertanggung</p>
                            <GuarantorTransactionForm data={premiDetail.tertanggung} />
                        </Col>
                        <Col lg={6}>
                            <p className="fw-bold text-center">Ahli waris</p>
                            <HierTransactionForm data={premiDetail.ahli_waris} relation={premiDetail.ahli_waris.relation} />
                        </Col>
                    </Row>
                    <Row className="">
                        <Col>
                            {premiDetail.status.status_id === 1 ? (
                                <div className="float-end m-2">
                                    <Button className="m-1 btn-success" onClick={acceptHandler}>
                                        Diterima
                                    </Button>
                                    <Button className="m-1 btn-danger" onClick={rejectHandler}>
                                        Ditolak
                                    </Button>
                                </div>
                            ) : (
                                <div></div>
                            )}
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        {premiDetail.status.status_id === 5 ? (
                            <Col className="m-2">
                                <h5 className="mt-1 p-2">Detail Pengajuan Claim</h5>
                                <ClaimTransactionForm
                                    claimImage={premiDetail.claim_image}
                                    premi={premiDetail.premi_id}
                                    status={premiDetail.status.status_id}
                                />
                            </Col>
                        ) : premiDetail.status.status_id === 6 ? (
                            <h5 className="mt-1 p-2 text-center text-secondary">
                                Pengajuan klaim telah diterima
                            </h5>
                        ) : premiDetail.status.status_id === 4 ? (
                            <h5 className="mt-1 p-2 text-center text-secondary">
                                Status premi di tolak, tidak dapat mengajukan klaim
                            </h5>
                        ) : (
                            <h5 className="mt-1 p-2 text-center text-secondary">
                                Belum ada klaim yang diajukan
                            </h5>
                        )}
                    </Row>
                </Col>
            </Card>
            </Col>
           
        </Container>
    );
};
export default DetailRequestPage;
