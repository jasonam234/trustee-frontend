import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { IoAddCircle } from "react-icons/io5";
import { Link } from "react-router-dom";
import HabitList from "../../components/admin/list/HabitList";
const HabitPage = () => {
    const style = { color: "white" };
    return (
        <div>
          <Container className="mt-7">
          <Row>
                <Col> <h5 className="mt-5 text-secondary">Jenis Kebiasaan</h5></Col>
                <Col>
                <Link to="/kebiasaan/tambah">
                <Button variant="primary mt-5 float-end" size="sm">
                        Tambah <IoAddCircle style={style} size={20} />
                    </Button>
                    </Link>
                </Col> 
            </Row>
          <Card className="table-responsive p-3 mt-3">
                <HabitList/>
            </Card>
          </Container>
        </div>
    );
};

export default HabitPage;
