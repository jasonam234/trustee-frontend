import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { IoAddCircle } from "react-icons/io5";
import InsuranceTypeList from "../../components/admin/list/InsuranceTypeList";
const InsurancePage = () => {
    const style = { color: "white" };
    return (
        <div>
            <Container className="mt-7">
                <Row>
                    <Col>
                        <h5 className="mt-5 text-secondary">Jenis Asuransi</h5>
                    </Col>
                    <Col>
                        <Link to="/jenis_asuransi/tambah">
                            <Button variant="primary mt-5 float-end" size="sm">
                                Tambah <IoAddCircle style={style} size={20} />
                            </Button>
                        </Link>
                    </Col>
                </Row>
                <Card className="table-responsive p-3 mt-3">
                    <InsuranceTypeList />
                </Card>
            </Container>
        </div>
    );
};

export default InsurancePage;
