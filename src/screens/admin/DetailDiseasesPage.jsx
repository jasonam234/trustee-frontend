import { Col, Container } from "react-bootstrap";
import DiseasesHistoryFomCMS from "../../components/admin/form/DiseasesHistoryFomCMS";
const DetailDiseasesPage = () => {
    return (
        <Container className="mt-7">
            <Col lg={8} className="m-auto mt-5">
                <DiseasesHistoryFomCMS />
            </Col>
        </Container>
    );
};
export default DetailDiseasesPage;
