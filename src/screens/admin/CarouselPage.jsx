import { Col, Container } from "react-bootstrap";
import React from "react";
import CarouselForm from "../../components/admin/form/CarouselForm";
const CarouselPage = () => {
    return (
        <Container className="mt-7">
            <Col lg={8}  lg={8} className="m-auto"> 
            <h5 className="mt-5 text-seconndary">Carousel</h5>
            <CarouselForm />
            </Col>
           
        </Container>
    );
};

export default CarouselPage;
