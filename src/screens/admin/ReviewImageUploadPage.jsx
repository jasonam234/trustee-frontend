import { Col,Container } from "react-bootstrap";
import React from "react";
import CarouselForm from "../../components/admin/form/CarouselForm";
const ReviewImageUploadPage = () => {
    return (
        <Container className="mt-7">
            <Col lg={8}  lg={8} className="m-auto muted"> 
            <h5 className="mt-5 text-secondary">Review</h5>
            <CarouselForm />
            </Col>
        </Container>
    );
};

export default ReviewImageUploadPage;
