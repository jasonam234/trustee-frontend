import { Col, Container } from "react-bootstrap";
import PremiPackageForm from "../../components/admin/form/PremiPackageForm";
const DetailPremiPackagePage = () => {
    return (
        <Container className="mt-7">
            <Col lg={8} className="m-auto mt-5 muted">
                <PremiPackageForm />
            </Col>
        </Container>
    );
};
export default DetailPremiPackagePage;
