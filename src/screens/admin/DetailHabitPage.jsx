import { Col, Container } from "react-bootstrap";
import HabitFormCMS from "../../components/admin/form/HabitFormCMS";
const DetailHabitPage = () => {
    return (
        <Container className="mt-7">
            <Col lg={8} className="m-auto mt-5">
                <HabitFormCMS />
            </Col>
        </Container>
    );
};
export default DetailHabitPage;
