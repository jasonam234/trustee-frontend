import { Container,Card } from "react-bootstrap";
import PremiList from "../../components/admin/list/PremiList";
const PremiListPage = () => {
    return (
        <div>
          <Container className="mt-7">
          <h5 className="mt-5 text-secondary">Daftar Premi</h5>
          <Card className="table-responsive p-3">
               <PremiList/>
            </Card>
          </Container>
        </div>
    );
};
export default PremiListPage;
