import NotFound from "../../components/global/NotFound"
const NotFoundPage =()=>{

    return(
        <div className="mt-5">
            <NotFound/>
        </div>
        
    )
}
export default NotFoundPage