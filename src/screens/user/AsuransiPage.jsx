import PremiSection from "../../components/user/AsuransiPage/PremiSection";
import CustomizedAccordions from "../../components/user/UserGuidePage/AccordionSection";
import { useEffect, useState } from "react";
import CustomLoader from "../../components/loader/CustomLoader";
import { useSelector } from "react-redux";
const AsuransiPage = () => {
  const [isLoading, setIsLoading] = useState(false);
  const refresh = useSelector((state) => {
    return state.page.refresh;
  });
  
  useEffect(() => {
    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
    }, 3000);
  }, isLoading,refresh);

  if (isLoading === true) {
    return (
      <div className="mt-5">
        <CustomLoader/>
      </div>
    )
  }
  return (
    <div style={{ marginTop: "6%" }}>
      <PremiSection />
      <CustomizedAccordions />
    </div>
  );
};

export default AsuransiPage;
