import { Container } from "reactstrap";
import BasicTabs from "../../components/user/PremiUserPage/TabSection";
const PremiUserPage = () => {
  return (
    <div style={{ marginTop: "5%" }}>
      <Container fluid>
        <BasicTabs/>
      </Container>
    </div>
  );
};

export default PremiUserPage;
