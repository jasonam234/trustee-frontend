import CallToActionSection from "../../components/user/LandingPage/CallToActionSection";
import CarouselPage from "../../components/user/LandingPage/CarouselPage"
import DiscountSection from "../../components/user/LandingPage/DiscountSection";
import TentangSection from "../../components/user/LandingPage/TentangSection";
import UserReviewSection from "../../components/user/LandingPage/UserReviewSection";

const LandingPage = () => {

    return (
        <div  className="mt-5">
            <CarouselPage />
            <TentangSection />
            <UserReviewSection/>
            <CallToActionSection/>
        </div>
    )
}

export default LandingPage;