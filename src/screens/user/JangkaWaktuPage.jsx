import { useParams } from "react-router";
import JangkaWaktuSection from "../../components/user/JangkaWaktuPage/JangkaWaktuSection";

const JangkaWaktuPage = () => {
    return(
        <div  style={{marginTop:"5%"}}>
            <JangkaWaktuSection/>
        </div>
    )
}

export default JangkaWaktuPage;