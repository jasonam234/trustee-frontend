import { useEffect, useState } from "react";
import { Container } from "reactstrap";
import LoadingSpin from "../../components/loader/LoadingSpin";
import AboutTentangSection from "../../components/user/AboutPage/AboutTentangSection";
import CallToActionSection from "../../components/user/LandingPage/CallToActionSection";

const AboutPage = () => {
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
    console.log("hi");
  }, isLoading);
  if (isLoading === true) {
    return <LoadingSpin />;
  }

  return (
    <div style={{marginTop:"6%"}}>
      {/* <CarouselPage/> */}
      <Container fluid>
        <AboutTentangSection />
        <CallToActionSection/>
      </Container>
    </div>
  );
};

export default AboutPage;
