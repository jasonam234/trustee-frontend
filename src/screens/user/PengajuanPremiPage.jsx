import FormPengajuan from "../../components/user/PengajuanPremi/FormPengajuan";

const PengajuanPremiPage = () => {
  return (
    <div className="mt-5">
      <FormPengajuan />
    </div>
  );
};

export default PengajuanPremiPage;
