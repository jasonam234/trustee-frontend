import CaraBeliSection from "../../components/user/UserGuidePage/CaraBeliSection";
import CaraKlaimSection from "../../components/user/UserGuidePage/CaraKlaimSection";

const UserGuidePage = () => {
    return (
        <div className="mt-5">
            <CaraBeliSection/>
            <CaraKlaimSection/>
        </div>        
    )
}

export default UserGuidePage;