import { Container } from "reactstrap";
import PengajuanKlaimSection from "../../components/user/PengajuanKlaimPage/PengajuanKlaimSection";

const PengajuanKlaimPage = () => {
  return (
    <div className="mt-5">
      <Container fluid>
        <PengajuanKlaimSection />
      </Container>
    </div>
  );
};
export default PengajuanKlaimPage;
