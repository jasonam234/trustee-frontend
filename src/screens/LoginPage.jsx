import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Card,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Button,
  Container,
  CardBody,
  FormFeedback,
  Col,
} from "reactstrap";
import { authActions } from "../store/auth";
import "../assets/LoginPage.css";
import { Link, useNavigate } from "react-router-dom";
import ReCAPTCHA from "react-google-recaptcha";
import React from "react";
import { RECAPTCHA_KEY } from "../base";
import { loginNew } from "../api/EndPointAuth";
import swal from "sweetalert";
import { pageActions } from "../store/page";

const LoginPage = () => {
  const isAuth = useSelector((state) => {
    return state.auth.isAuth;
  });
  const isAdmin = useSelector((state) => {
    return state.auth.isAdmin;
  });
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const [form, setForm] = useState({
    username: "",
    password: "",
  });
  const formHandler = (evt) => {
    setForm((prevsValue) => {
      return {
        ...prevsValue,
        [evt.target.name]: evt.target.value,
      };
    });
  };
  const loginHandler = async (evt) => {
    evt.preventDefault();
    if (form.username === "" || form.password === "") {
      swal("Gagal!", "Username dan Password tidak boleh kosong", "error");
      return;
    } else {
      loginNew(form)
        .then((response) => {
          dispatch(authActions.login(response.data.data));
          navigate("/");
          dispatch(pageActions.refresh(1));
          // window.location.reload();
        })
        .catch(() => {
          swal("Gagal!", "Username atau Password salah!", "error");
        });
    }
  };

  //recapthca
  const [isVerify, setVerify] = useState(false);
  const verifyHandler = () => {
    setVerify(true);
  };
  return (
    <div>
      <Container fluid>
        <Row className="rowCenter">
          <Col lg={4} className="m-auto mt-5">
            <Card className="cardLogin">
              <CardBody>
                <h3 className="textLoginHead">Trustee</h3>
                <Form onSubmit={loginHandler} style={{ textAlign: "left" }}>
                  <FormGroup floating>
                    <Input
                      type="email"
                      name="username"
                      onChange={formHandler}
                      placeholder="Enter email..."
                      valid={false}
                    />
                    <Label for="userEmail">Email </Label>
                    <FormFeedback valid>
                      Sweet! that name is available
                    </FormFeedback>
                  </FormGroup>
                  <FormGroup floating>
                    <Input
                      name="password"
                      type="password"
                      onChange={formHandler}
                      placeholder="Enter password..."
                    />
                    <Label for="userPassword">Password </Label>
                  </FormGroup>
                  <div style={{ textAlign: "center" }}>
                    <div style={{ display: "inline-block", marginTop: "20px" }}>
                      <ReCAPTCHA
                        sitekey={RECAPTCHA_KEY}
                        onChange={verifyHandler}
                      />
                    </div>
                  </div>
                  <br />
                  <Button
                    className="btnL btnLogin mgBtmLgn"
                    type="submit"
                    disabled={!isVerify}
                  >
                    Masuk
                  </Button>
                </Form>
                <br />
                <Link
                  to="/register"
                  style={{
                    textDecoration: "none",
                  }}
                >
                  Daftar!
                </Link>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default LoginPage;
