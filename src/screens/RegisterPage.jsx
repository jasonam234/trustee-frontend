import { useState } from "react";
import { Card, Row, Button, Container, Col } from "react-bootstrap";
import { Form, FormGroup, FormFeedback, Label, Input } from "reactstrap";
import "../assets/RegisterPage.css";
import { Link } from "react-router-dom";
import ReCAPTCHA from "react-google-recaptcha";
import { authActions } from "../store/auth";
import { useDispatch, useSelector } from "react-redux";
import { RECAPTCHA_KEY } from "../base";
import { useNavigate } from "react-router";
import { register, registerNew } from "../api/EndPointAuth";
import swal from "sweetalert";

const RegisterPage = () => {
  const navigate = useNavigate();
  const [form, setForm] = useState({
    role: {
      role_id: 1,
    },
    username: "",
    full_name: "",
    phone: "",
    password: "",
  });
  const formHandler = (evt) => {
    let value = evt.target.value;
    let name = evt.target.name;

    if (name === "password") {
      let regexPassword = new RegExp(
        "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$"
      );
      if (value === "") {
        setIsCorrect2(false);
        // setVerify(false);
      } else if (!regexPassword.test(value)) {
        setIsCorrect2(true);
        setisExist2(false);
      } else {
        setisExist2(true);
        setIsCorrect2(false);
      }
    } else if (name === "username") {
      let regexUsername = new RegExp("[A-Za-z0-9]+[@]+[a-z]+[.]+[c][o][m]");
      if (value === "") {
        setIsCorrect3(false);
        // setVerify(false);
      } else if (!regexUsername.test(value)) {
        setIsExist(true);
        setIsCorrect3(false);
      } else {
        setIsExist(false);
        setIsCorrect3(true);
      }
    } else if (name === "full_name") {
      let regexName = new RegExp("[a-zA-Z ]+$");
      if (value === "") {
        // setVerify(false);
      } else if (!regexName.test(value)) {
        setIsExist3(true);
      } else {
        setIsExist3(false);
      }
    } else if (name === "phone") {
      let regexNoHp = new RegExp("[0-9]$");
      if (value === "") {
        // setVerify(false);
        setIsCorrect4(false);
      } else if (!regexNoHp.test(value)) {
        setCorrect(true);
      } else {
        setIsCorrect4(true);
        setCorrect(false);
      }
    }
    setForm((prevsValue) => {
      return {
        ...prevsValue,
        [evt.target.name]: evt.target.value,
      };
    });
  };

  //recapthca
  const [isVerify, setVerify] = useState(false);
  const verifyHandler = () => {
    setVerify(true);
  };
  const [isCorrect, setCorrect] = useState(false);
  const [isCorrect2, setIsCorrect2] = useState(false);
  const [isCorrect3, setIsCorrect3] = useState(false);
  const [isCorrect4, setIsCorrect4] = useState(false);
  const [isExist, setIsExist] = useState(false);
  const [isExist2, setisExist2] = useState(false);
  const [isExist3, setIsExist3] = useState(false);

  const registerHandler = (evt) => {
    evt.preventDefault();

    if (
      form.username === "" ||
      form.password === "" ||
      form.full_name === "" ||
      form.phone === ""
    ) {
      swal("Gagal!", "Field tidak boleh kosong", "error");
    }
    else {
      registerNew(form)
        .then((res) => {
          swal("Berhasil!", "Register berhasil!", "success");
          console.log(res);
          navigate("/login");
        })
        .catch((err) => {
          swal("Gagal!", "Terdapat username yang sama!", "error");
          navigate("/register");
        });
    }
  };
  return (
    <div>
      <Container fluid>
        <Row className="rowCenter">
          <Col lg={4} className="m-auto mt-5">
            <Card className="cardLogin">
              <Card.Body>
                <h3 className="textRegHead">Form Pendaftaran</h3>
                <Form style={{ textAlign: "left" }}>
                  <FormGroup floating>
                    <Input
                      type="email"
                      name="username"
                      onChange={formHandler}
                      placeholder="Enter email..."
                      invalid={isExist}
                      valid={isCorrect3}
                    />
                    <Label for="userEmail">Email </Label>
                    <FormFeedback>Username harus berupa email</FormFeedback>
                    <FormFeedback valid>Username dapat digunakan!</FormFeedback>
                  </FormGroup>
                  <FormGroup floating>
                    <Input
                      type="password"
                      name="password"
                      onChange={formHandler}
                      placeholder="Masukan password anda..."
                      invalid={isCorrect2}
                      valid={isExist2}
                    />
                    <Label for="userPassword">Password </Label>
                    <FormFeedback>
                      Password harus berisikan minimal 1 huruf besar, 1 angka,
                      dan 1 simbol
                    </FormFeedback>
                    <FormFeedback valid>Password bisa digunakan </FormFeedback>
                    {/* <Form.Text>Masukan password anda!.</Form.Text> */}
                  </FormGroup>
                  <FormGroup floating>
                    <Input
                      // className="InputLogin"
                      type="text"
                      name="full_name"
                      onChange={formHandler}
                      placeholder="Masukan nama anda..."
                      invalid={isExist3}
                    />
                    <Label for="userName">Nama </Label>
                    <FormFeedback>
                      Nama harus berupa huruf dan tidak boleh angka
                    </FormFeedback>
                  </FormGroup>
                  <FormGroup floating>
                    <Input
                      type="text"
                      name="phone"
                      onChange={formHandler}
                      placeholder="Masukan nomor handphone anda..."
                      invalid={isCorrect}
                      valid={isCorrect4}
                    />
                    <Label for="userPhone">No Hp </Label>
                    <FormFeedback>
                      No HP Tidak boleh terdapat huruf
                    </FormFeedback>
                  </FormGroup>
                  <br />

                  <div style={{ textAlign: "center" }}>
                    <div style={{ display: "inline-block" }}>
                      <ReCAPTCHA
                        sitekey={RECAPTCHA_KEY}
                        onChange={verifyHandler}
                      />
                    </div>
                  </div>
                  {/* <ReCAPTCHA sitekey={RECAPTCHA_KEY} onChange={verifyHandler} /> */}
                  <br />
                  <Button
                    className="btnR btnReg mgBtmReg"
                    onClick={registerHandler}
                    disabled={!isVerify}
                  >
                    Daftar
                  </Button>
                </Form>
                <Link to="/login">
                  <Button className="btnR btnReg mgBtmReg">
                    Kembali ke Login
                  </Button>
                </Link>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default RegisterPage;
