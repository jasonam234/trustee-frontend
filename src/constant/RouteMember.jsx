import { Route, Routes } from "react-router-dom";
import NavbarPage from "../components/user/NavbarPage";
import AsuransiPage from "../screens/user/AsuransiPage";
import JangkaWaktuPage from "../screens/user/JangkaWaktuPage";
import PengajuanPremiPage from "../screens/user/PengajuanPremiPage";
import PremiUserPage from "../screens/user/PremiUserPage";
import UserGuidePage from "../screens/user/UserGuidePage";
import LandingPage from "../screens/user/LandingPage";
import PengajuanKlaimPage from "../screens/user/PengajuanKlaimPage";
import FooterPage from "../components/global/FooterPage";
import AboutPage from "../screens/user/AboutPage";
import NotFound from "../components/global/NotFound";

const RouteMember = () => {
  return (
    <>
      <NavbarPage />
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="*" element={<NotFound/>} />
        <Route path="/guide" element={<UserGuidePage />} />
        <Route path="/asuransi/:premi_id" element={<AsuransiPage />} />
        <Route
          path="/asuransi/:premi_id/:paket_name"
          element={<JangkaWaktuPage />}
        />
        <Route
          path="/asuransi/form/:premi_id"
          element={<PengajuanPremiPage />}
        />
        <Route path="/user/klaim/premi/:premi_id" element={<PengajuanKlaimPage/>} />
        {/* <Route path="/user/profile" element={<ProfileUserPage />} /> */}
        <Route path="/user/premi" element={<PremiUserPage />} />
        <Route path ="/about" element={<AboutPage/>} />
        <Route path="/asuransi/:premi_id" element={<AsuransiPage />} />
      </Routes>
      <FooterPage/>
    </>
  );
};

export default RouteMember;
