import AdminDashboardPage from "../screens/admin/AdminDashboardPage";
import ContentPage from "../screens/admin/ContentPage";
import ReportPage from "../screens/admin/ReportPage";
import RelationshipPage from "../screens/admin/RelationshipPage";
import InsurancePage from "../screens/admin/InsurancePage";
import PremiPage from "../screens/admin/PremiPage";
import { Route ,Routes} from "react-router";
import NavbarAdmin from "../components/admin/NavbarAdmin/NavbarAdmin";
import DetailInsuranceType from "../screens/admin/DetailInsuranceTypePage";
import DetailRelationPage from "../screens/admin/DetailRelationPage";
import DetailPremiPackagePage from "../screens/admin/DetailPremiPackagePage";
import ClaimDetailPage from "../screens/admin/ClaimDetailPage";
import DetailRequestPage from "../screens/admin/DetailRequestPage";
import CarouselPage from "../screens/admin/CarouselPage";
import ReviewPage from "../screens/admin/ReviewPage";
import FooterAdmin from "../components/admin/footer/FooterAdmin";
import PremiListPage from "../screens/admin/PremiListPage";
import ClaimListPage from "../screens/admin/ClaimListPage";
import NotFound from "../components/global/NotFound";
import ReviewImageUploadPage from "../screens/admin/ReviewImageUploadPage";
import DiseasesHistoryPage from "../screens/admin/DiseasesHistoryPage";
import HabitPage from "../screens/admin/HabitPage";
import DetailDiseasesPage from "../screens/admin/DetailDiseasesPage";
import DetailHabitPage from "../screens/admin/DetailHabitPage";
const RouteAdmin =()=>{
    return(
        <>
        <NavbarAdmin/>
        <Routes>
                <Route path="/" element={<AdminDashboardPage />} />
                <Route path="/admin" element={<AdminDashboardPage />} />
                <Route path="/premi"element={<PremiListPage />} />
                <Route path="/premi/:id" element={<DetailRequestPage />} />
                <Route path="/klaim"element={<ClaimListPage />} />
                <Route path="/klaim/:id" element={<ClaimDetailPage />} />
                <Route path="/jenis_asuransi" element={<InsurancePage />} />
                <Route path="/jenis_asuransi/:idJenis" element={<DetailInsuranceType />} />
                <Route path="/hubungan" element={<RelationshipPage />} />
                <Route path="/hubungan/:idHubungan" element={<DetailRelationPage />} />
                <Route path="/hubungan/tambah" element={<DetailRelationPage />} />
                <Route path="/penyakit" element={<DiseasesHistoryPage />} />
                <Route path="/penyakit/:idPenyakit" element={<DetailDiseasesPage />} />
                <Route path="/penyakit/tambah" element={<DetailDiseasesPage />} />
                <Route path="/kebiasaan" element={<HabitPage />} />
                <Route path="/kebiasaan/:idKebiasaan" element={<DetailHabitPage />} />
                <Route path="/kebiasaan/tambah" element={<DetailHabitPage />} />
                <Route path="/paket_premi" element={<PremiPage />} />
                <Route path="/paket_premi/:idPremi" element={<DetailPremiPackagePage />} />
                <Route path="/paket_premi/tambah" element={<DetailPremiPackagePage />} />
                <Route path="/laporan" element={<ReportPage />} />
                <Route path="/konten" element={<ContentPage />} />
                <Route path="/konten/carousel/:id" element={<CarouselPage />} />
                <Route path="/konten/review/:id" element={<ReviewPage />} />
                <Route path="/konten/review-gambar/:id" element={<ReviewImageUploadPage />} />
                <Route path="*" element={<NotFound />} />
        </Routes>
        <FooterAdmin/>
        </>
        
    )
}
export default RouteAdmin;