import { Route, Routes, useLocation } from "react-router";
import FooterPage from "../components/global/FooterPage";
import NavbarPage from "../components/user/NavbarPage";
import LoginPage from "../screens/LoginPage";
import RegisterPage from "../screens/RegisterPage";
import AboutPage from "../screens/user/AboutPage";
import AsuransiPage from "../screens/user/AsuransiPage";
import JangkaWaktuPage from "../screens/user/JangkaWaktuPage";
import LandingPage from "../screens/user/LandingPage";
import UserGuidePage from "../screens/user/UserGuidePage";

const RouteNonMember = () => {
  const location = useLocation();
  const [, checkLocation] = location.pathname.split("/");
  return (
    <>
      {checkLocation === "login" || checkLocation === "register" ? (
        <div>
          <Routes>
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
          </Routes>
        </div>
      ) : (
        <>
          <NavbarPage />
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route path="*" element={<LandingPage />} />
            <Route path="/guide" element={<UserGuidePage />} />
            <Route path="/asuransi/:premi_id" element={<AsuransiPage />} />
            <Route
              path="/asuransi/:premi_id/:paket_name"
              element={<JangkaWaktuPage />}
            />
            <Route path="/about" element={<AboutPage />} />
          </Routes>
          <FooterPage />
        </>
      )}
    </>
  );
};

export default RouteNonMember;
