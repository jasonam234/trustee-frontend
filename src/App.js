import "./App.css";
import "./assets/global/Global.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import { useSelector, useDispatch } from "react-redux";
import { Fragment, useEffect } from "react";
import { useNavigate } from "react-router";
import RouteAdmin from "./constant/RouteAdmin";
import RouteMember from "./constant/RouteMember";
import RouteNonMember from "./constant/RouteNonMember";
import { authActions } from "./store/auth";

function App() {
    let navigate = useNavigate();
    const dispatch = useDispatch();
    const isAuth = useSelector((state) => {
        return state.auth.isAuth;
    });
    const isAdmin = useSelector((state) => {
        return state.auth.isAdmin;
    });

    const role = localStorage.getItem("role");
    const token = localStorage.getItem("token");
    //clear session
    const exp = localStorage.getItem("exp");
    const expiryDate = new Date(exp * 1000);
    const now = Date.now();
    const dates = new Date(now);

    const sessionCheck = () => {
        if (token && dates >= expiryDate) {
            localStorage.clear();
            window.location.reload();
            navigate("/login");
        }
    };

    const authCheck = async () => {
        if (token && role === "admin") {
            await dispatch(authActions.asAdmin);
            <RouteAdmin />;
        } else if (token && role === "user") {
            await dispatch(authActions.asUser);
            <RouteMember />;
        } else if (!token) {
          <RouteNonMember />
        }
    };

    useEffect(() => {
        const getAuthCheck = async () => {
            await authCheck();
            sessionCheck();
        };
        getAuthCheck();
    }, [isAuth, isAdmin]);

    return (
        <Fragment>
            {token && role === "admin" ? (
                <RouteAdmin />
            ) : token && role === "user" ? (
                <RouteMember />
            ) : (
                <RouteNonMember />
            )}
        </Fragment>
    );
}

export default App;
