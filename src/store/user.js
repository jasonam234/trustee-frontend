import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    ahliWaris : {},
    tertanggung : {},
    paket : {},
};

const userSlice = createSlice({
    name:"user",
    initialState : initialState,
    reducers : {
        ahliWaris(state,data) {
            state.ahliWaris = data.payload;
        },
        tertanggung(state,data) {
            state.tertanggung = data.payload;
        },
        paket(state,data) {
            state.paket = data.payload;
        },
        // syaratKesahatan(state,data) {
        //     state.syaratKesahatan = data.payload;
        // }
    }
})

export const userActions = userSlice.actions;
export default userSlice.reducer;