import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    insuranceType : [],
    insurancePackage:[]
};

const insuranceSlice = createSlice({
    name:"insurance",
    initialState : initialState,
    reducers : {
        allInsuranceType(state,data) {
            state.insuranceType = data.payload;
        },
        allInsurancePackage(state,data) {
            state.insurancePackage = data.payload;
        },
        
    }
})

export const insuranceActions = insuranceSlice.actions;
export default insuranceSlice.reducer;