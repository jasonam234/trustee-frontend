import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    relation : [],
    habit : [],
    diseases : [],
};

const formSlice = createSlice({
    name:"relation",
    initialState : initialState,
    reducers : {
        allRelation(state,data) {
            state.relation = data.payload;
        },
        allHabit(state,data) {
            state.habit = data.payload;
        },
        allDiseases(state,data) {
            state.diseases = data.payload;
        },
        
    }
})

export const formActions = formSlice.actions;
export default formSlice.reducer;