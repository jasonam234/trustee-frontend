import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    cmsPage :[],
    countPremi :0,
    refresh:0
};

const pageSlice = createSlice ({
    name : "page",
    initialState : initialState,
    reducers: {
        cms(state,data) {
            state.cmsPage = data.payload;
        },
        countPremi(state,data) {
            state.countPremi = data.payload;
        },
        refresh(state,data) {
            state.refresh = state.refresh + data.payload;
        }
    }
})

export const pageActions = pageSlice.actions;
export default pageSlice.reducer;