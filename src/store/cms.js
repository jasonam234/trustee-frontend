import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    carousel : [],
    reviews :[],
    reviewsById :[],

};

const cmsSlice = createSlice({
    name:"cms",
    initialState : initialState,
    reducers : {
        allCarousel(state,data) {
            state.cms = data.payload;
        },
        allReview(state,data) {
            state.reviews = data.payload;
        },
        reviewById(state,data) {
            state.reviewsById = data.payload;
        },
    }
})

export const cmsActions = cmsSlice.actions;
export default cmsSlice.reducer;