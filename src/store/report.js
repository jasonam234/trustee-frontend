import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    report :[],
};

const reportSlice = createSlice ({
    name : "report",
    initialState : initialState,
    reducers: {
        getReport(state,data) {
            state.report = data.payload;
        },
    }
})

export const reportActions = reportSlice.actions;
export default reportSlice.reducer;