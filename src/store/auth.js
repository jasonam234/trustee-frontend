import { createSlice } from "@reduxjs/toolkit";
import jwt_decode from "jwt-decode";

const initialState = {
    isAuth:false,
    userDetail: {},
    token: null,
    isError : null,
    isAdmin:false,
    isVerify:false
};

const authSlice = createSlice ({
    name : "auth",
    initialState : initialState,
    reducers: {
        login(state, data) {
            localStorage.setItem("token", data.payload);
            let decode = jwt_decode(data.payload);
            localStorage.setItem("user_id", decode.user_id);
            localStorage.setItem("role", decode.role);
            localStorage.setItem("exp", decode.exp);
            state.userDetail = decode;
            state.token = data.payload
            state.isAuth = true;
        },
        logout(state) {
            localStorage.clear();
        },

        asAdmin(state) {
            state.isAdmin = true;
        },
        asUser(state) {
            state.isAdmin = false;
        },
        verify(state) {
            state.isVerify = true;
        }
    }
})

export const authActions = authSlice.actions;
export default authSlice.reducer;