import { configureStore } from "@reduxjs/toolkit";
import auth from "./auth";
import page from "./page";
import user from "./user";
import premi from "./premi";
import cms from "./cms";
import insurance from "./insurance";
import form from "./form";
import report from "./report";

const store = configureStore ({
    reducer : {
        auth : auth,
        page : page,
        user : user,
        premi : premi,
        cms : cms,
        form:form,
        insurance : insurance,
        report : report,
    },
})

export default store;