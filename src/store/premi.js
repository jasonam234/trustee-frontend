import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    premi : [],
    claim: [],
    premiDetail:{},
    tertanggung:{},
    ahliWaris:{},
    paket:{},
    jenis:{},
    user:{},
    status:{},
    relation:{}
};

const premiSlice = createSlice({
    name:"premi",
    initialState : initialState,
    reducers : {
        allPremi(state,data) {
            state.premi = data.payload;
        },
        premiDetail(state,data) {
            state.premiDetail = data.payload;
        },
        allClaim(state,data) {
            state.claim = data.payload;
        },
        getGuarantor(state,data) {
            state.tertanggung = data.payload;
        },
        getHeir(state,data) {
            state.ahliWaris = data.payload;
        },
        getPackage(state,data) {
            state.paket = data.payload;
        },
        getType(state,data) {
            state.jenis = data.payload;
        },
        getUser(state,data) {
            state.user = data.payload;
        },
        getStatus(state,data) {
            state.status= data.payload;
        },
        getRelation(state,data) {
            state.relation= data.payload;
        },
        
    }
})

export const premiActions = premiSlice.actions;
export default premiSlice.reducer;